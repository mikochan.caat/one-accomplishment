﻿$ErrorActionPreference = "Stop"

function Wait-Pause {
    echo ""
    echo "Press any key to continue..."
    cmd.exe /c "pause" | Out-Null
}

function Wait-Exit {
    Wait-Pause
    exit
}

function Write-Notice {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Green
}

function Write-NoticeUI {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor DarkGreen
}

function Write-Warn {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Yellow
}

function Write-WarnUI {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor DarkYellow
}

function Write-Critical {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Red
}
