#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package)
$ErrorActionPreference = 'Stop'

function Register-Module {
    param (
        [Parameter(Mandatory=$true)][string]$ModulePath
    )
    Import-Module (Join-Path "$($ToolsPath)\modules" $ModulePath) `
        -Force `
        -ArgumentList $InstallPath, $ToolsPath, $Package
}

Register-Module "workspace.psm1"
