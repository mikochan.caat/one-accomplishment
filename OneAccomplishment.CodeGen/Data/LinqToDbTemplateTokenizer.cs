using System;
using System.Text.RegularExpressions;
using Fasterflect;
using Kodeo.Reegenerator.Generators;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Data
{
    [TemplateDisplay(
        DisplayName = "Linq2DB Template Tokenizer",
        Description = "Tokenizes all strings of a Linq2DB template that are enclosed in <%= %>.")]
    [DefaultExtension(Extension = ".generated.cs")]
    public sealed class LinqToDbTemplateTokenizer : CodeRenderer
    {
        private const string TagRegexPattern = @"<%=\s*(?<propertyName>.+?)\s*%>";

        private static readonly string TokenizerRegexPattern = 
            String.Format("((?<quoted>\"{0}\")|//{0})", TagRegexPattern);
        private static readonly Regex TokenizerRegex = 
            new Regex(TokenizerRegexPattern, RegexOptions.Multiline);

        public override RenderResults Render()
        {
            string fileContents = this.ProjectItem.GetCurrentContentAsString();
            string tokenizedTemplate = TokenizerRegex.Replace(fileContents, (match) => {
                string propertyName = match.Groups["propertyName"].Value;
                string propertyValue;
                try {
                    object valueObject = T4Configuration.Instance.TryGetPropertyValue(propertyName);
                    if (valueObject == null) {
                        return match.Value;
                    }
                    propertyValue = (string)valueObject;
                } catch (InvalidCastException ex) {
                    string errorMessage = String.Format("Property '{0}' is not a string.", propertyName);
                    throw new InvalidOperationException(errorMessage, ex);
                }

                if (match.Groups["quoted"].Success) {
                    return "\"" + propertyValue + "\"";
                }
                return propertyValue;
            });

            var customToolResults = this.RunOtherCustomTool(
                Generators.TextTemplatingFileGenerator,
                this.ProjectItem.FullPath,
                tokenizedTemplate,
                this.ProjectItem.CodeNamespace);
            var renderResult = new RenderResults(customToolResults, RenderResults.DefaultEncoding);
            renderResult.AdditionalResults.Add(new AdditionalFile("tokenized.t4", tokenizedTemplate));
            
            return renderResult;
        }
    }
}
