﻿using System;
using System.Linq;
using LinqToDB.Data;
using LinqToDB.DataProvider.SqlServer;
using Net40Utilities.Validation;

namespace OneAccomplishment.CodeGen.Data
{
    internal static class LinqToDbHelpers
    {
        private static int ValidNameDuplicateCounter = 0;

        public static DataConnection GetSqlServerConnection()
        {
            return GetSqlServerConnection(
                T4Configuration.Instance.DatabaseServer,
                T4Configuration.Instance.DatabaseCatalog);
        }

        public static DataConnection GetSqlServerConnection(string server, string database)
        {
            string connectionString = String.Format(
                "Data Source={0};Database={1};Integrated Security=SSPI", 
                server, 
                database);
            return GetSqlServerConnection(connectionString);
        }

        public static DataConnection GetSqlServerConnection(string connectionString)
        {
            return SqlServerTools.CreateDataConnection(connectionString);
        }

        public static string ToValidName(string name)
        {
            ArgumentGuard.For(() => name).IsNullOrWhiteSpace().Throw();

            if (name.Contains("_")) {
                name = SplitAndJoin(name, "", '_');
            }
            if (name.Contains(".")) {
                name = SplitAndJoin(name, "", '.');
            }
            if (name.Length > 0 && Char.IsDigit(name[0])) {
                name = "_" + name;
            }
            if (String.IsNullOrEmpty(name)) {
                name = "_" + ++ValidNameDuplicateCounter;
            }

            return Char.ToUpper(name[0]) + name.Substring(1);
        }

        public static string StripNullableFromType(string type)
        {
            return type.Replace("?", "");
        }

        private static string SplitAndJoin(string value, string join, params char[] split)
        {
            var ss = value
                .Split(split, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => Char.ToUpper(s[0]) + s.Substring(1));
            return String.Join(join, ss.ToArray());
        }
    }
}
