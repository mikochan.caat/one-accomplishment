using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using Kodeo.Reegenerator.Generators;
using OneAccomplishment.CodeGen.Extensions;
using WrappedProjectItem = Kodeo.Reegenerator.Wrappers.ProjectItem;

namespace OneAccomplishment.CodeGen.Site
{
    [TemplateDisplay(
        DisplayName = "RequireJs Generator Renderer", 
        Description = "Generates all RequireJs module constants for C# and TypeScript.")]
    [DefaultExtension(Extension = ".log")]
    public sealed class RequireJsGeneratorRenderer : CodeRenderer
    {
        private IReadOnlyList<string> includedProjectPaths;
        private HashSet<string> acceptedFileExtensions;

        public override void PreRender()
        {
            base.PreRender();
            this.includedProjectPaths = this.ProjectItem.Project.TransformPaths(new string[] {
                "Scripts"
            });
            this.acceptedFileExtensions = new HashSet<string>(StringComparer.OrdinalIgnoreCase) {
                ".js", ".ts"
            };
        }

        public override RenderResults Render()
        {
            this.GetScriptItemsMetadata();

            return new RenderResults(this.Output.ToString());
        }

        private void GetScriptItemsMetadata()
        {
            var dteProject = this.ProjectItem.VsProjectItem.ContainingProject;
            var scriptProjectItems = dteProject.GetAllProjectItems()
                .Where(item => item.Kind == Constants.vsProjectItemKindPhysicalFile)
                .Select(item => WrappedProjectItem.FromProjectItem(item))
                .Where(this.FilterProjectItems)
                .ToArray();

            this.Output.WriteLine(String.Join("\r\n", scriptProjectItems.Select(z => z.CodeNamespaceRelative)));
        }

        private bool FilterProjectItems(WrappedProjectItem item)
        {
            return this.acceptedFileExtensions.Contains(item.Extension) &&
                this.includedProjectPaths.Any(
                    path => item.Directory.StartsWith(path, StringComparison.OrdinalIgnoreCase));
        }
    }
}
