using Kodeo.Reegenerator.Generators;

namespace OneAccomplishment.CodeGen.BusinessRules
{
    [TemplateDisplay(
        DisplayName = "Rule Violations Renderer",
        Description = "Sets up the IRuleStringResource as a rule violation.")]
    public sealed class RuleViolationsRenderer : AbstractRuleStringRenderer
    {
        public override string GenericBaseClassName
        {
            get { return "AbstractRuleViolation"; }
        }

        public override string CommonResourceFileName
        {
            get { return "CommonViolations"; }
        }
    }
}
