using System.Collections.Generic;
using System.IO;
using Kodeo.Reegenerator.Wrappers;
using OneAccomplishment.CodeGen.Core;
using OneAccomplishment.CodeGen.Extensions;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.BusinessRules
{
    public abstract partial class AbstractRuleStringRenderer 
        : IClassRenderer, IRendererProjectItem, IRendererOutput
    {
        public string ClassName { get; set; }
        public string ValidClassName { get; set; }
        public IEnumerable<ResourceStringData> CommonResourceStringData { get; private set; }
        public IEnumerable<ResourceStringData> ItemResourceStringData { get; private set; }
        public abstract string GenericBaseClassName { get; }
        public abstract string CommonResourceFileName { get; }

        ProjectItem IRendererProjectItem.ProjectItem
        {
            get { return this.ProjectItem; }
        }

        StringWriter IRendererOutput.Output
        {
            get { return this.Output; }
        }

        public override void PreRender()
        {
            base.PreRender();
            this.InitializeClassRendererMembers();
            this.RenderHeader();
            this.CommonResourceStringData = this.GetResourceStringData(this.CommonResourceFileName);
            this.ItemResourceStringData = this.GetResourceStringData(this.ClassName);
        }

        private IEnumerable<ResourceStringData> GetResourceStringData(string resourceFileName)
        {
            string resourcePath = Path.Combine(this.ProjectItem.Directory, resourceFileName + ".resx");
            return ResourceStringData.FromFile(resourcePath);
        }
    }
}
