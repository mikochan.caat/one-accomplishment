using Kodeo.Reegenerator.Generators;

namespace OneAccomplishment.CodeGen.BusinessRules
{
    [TemplateDisplay(
        DisplayName = "Rule Messages Renderer",
        Description = "Sets up the IRuleStringResource as a rule message.")]
    public sealed class RuleMessagesRenderer : AbstractRuleStringRenderer
    {
        public override string GenericBaseClassName
        {
            get { return "AbstractRuleStringResource"; }
        }

        public override string CommonResourceFileName
        {
            get { return "CommonMessages"; }
        }
    }
}
