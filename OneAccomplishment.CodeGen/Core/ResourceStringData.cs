﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace OneAccomplishment.CodeGen.Core
{
    public sealed class ResourceStringData
    {
        public string Name { get; private set; }
        public string ValidName { get; private set; }
        public string Value { get; private set; }
        public string Comment { get; private set; }

        public static IEnumerable<ResourceStringData> FromFile(string resxPath)
        {
            var doc = new XmlDocument();
            doc.Load(resxPath);
            var resourceStringData =  doc
                .SelectNodes("//data")
                .Cast<XmlElement>()
                .Where(xe => xe.Attributes["type"] == null)
                .Select(xe => new ResourceStringData {
                    Name = xe.Attributes["name"].Value,
                    Value = xe.SelectSingleNode("value").InnerText,
                    Comment = xe.SelectSingleNode("comment").InnerText
                });
            foreach (var data in resourceStringData) {
                data.ValidName = CSharpLanguage.CreateValidIdentifier(data.Name);
                yield return data;
            }
        }
    }
}
