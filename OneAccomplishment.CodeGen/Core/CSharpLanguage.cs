﻿using System;
using System.CodeDom.Compiler;
using System.Text.RegularExpressions;
using Net40Utilities.Validation;

namespace OneAccomplishment.CodeGen.Core
{
    internal static class CSharpLanguage
    {
        private static readonly CodeDomProvider CSharpProvider = CodeDomProvider.CreateProvider("C#");
        private static readonly Regex UnicodeCharsRegex = 
            new Regex(@"[^\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}\p{Nl}\p{Mn}\p{Mc}\p{Cf}\p{Pc}\p{Lm}]");
        private static readonly Regex IdentifierCleanerRegex =
            new Regex(@"(^\s*(?<firstChar>[^\s])|(?<whitespace>\s))");

        public static string CreateValidIdentifier(string identifier)
        {
            ArgumentGuard.For(() => identifier).IsNullOrWhiteSpace().Throw();

            if (!CSharpProvider.IsValidIdentifier(identifier)) {
                identifier = UnicodeCharsRegex.Replace(identifier, String.Empty);
            }

            return IdentifierCleanerRegex.Replace(identifier, (match) => {
                if (match.Groups["whitespace"].Success) {
                    return String.Empty;
                }

                var firstCharMatch = match.Groups["firstChar"];
                if (firstCharMatch.Success && !Char.IsLetter(firstCharMatch.Value, 0)) {
                    return "_" + firstCharMatch.Value;
                }

                return match.Value;
            });
        }
    }
}
