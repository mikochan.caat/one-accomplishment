﻿namespace OneAccomplishment.CodeGen.Renderers
{
    public interface IClassRenderer
    {
        string ClassName { get; set; }
        string ValidClassName { get; set; }
    }
}
