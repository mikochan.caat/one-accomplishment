﻿using System.IO;

namespace OneAccomplishment.CodeGen.Renderers
{
    public interface IRendererOutput
    {
        StringWriter Output { get; }
    }
}
