@echo off
REM -- Configurables --
set ext_folder=.ext
set folder_name_file=folder-name.txt
REM -- End --

call "%ext_folder%\Get-Folder-Name" "%ext_folder%\%folder_name_file%"

pushd "%ext_folder%"
set /p solution_dir=< "%folder_name_file%"
del "%folder_name_file%"
popd

set ws_path=..\%solution_dir%
"%ext_folder%\runner" "%ws_path%"
