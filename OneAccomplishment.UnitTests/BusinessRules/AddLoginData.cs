﻿using System;
using Moq;
using NUnit.Framework;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Commands.Logins;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.UnitTests.BusinessRules
{
    [TestFixture]
    public sealed class AddLoginData
    {
        [TestCase(null)]
        [TestCase("")]
        public void Handles_NullOrEmpty_Username(string username)
        {
            var domainService = new Mock<IDomainService>();
            var command = new AddLoginDataCommand {
                Username = username
            };
            var violation = Assert.Throws<BusinessRuleViolationException<LoginViolations>>(() => {
                var rule = new AddLoginDataCommandRules(domainService.Object);
                rule.ValidateCommand(command);
            });
            Assert.AreEqual(LoginViolations.MissingRequiredValue, violation.ViolatedRule);
            Assert.AreEqual(NameOf.Field(() => command.Username), violation.CommandProperty);
        }

        [TestCase("test_user")]
        public void Handles_Existing_Username(string username)
        {
            var domainService = new Mock<IDomainService>();
            domainService
                .Setup(x => x.IsUsernameExisting(It.IsAny<string>()))
                .Returns<string>(input => input == "test_user");

            Assert.DoesNotThrow(() => {
                var rule = new AddLoginDataCommandRules(domainService.Object);
                rule.ValidateCommand(new AddLoginDataCommand {
                    Username = username
                });
            });
        }

        [TestCase("nonexistent_user")]
        public void Handles_NonExisting_Username(string username)
        {
            var domainService = new Mock<IDomainService>();
            domainService
                .Setup(x => x.IsUsernameExisting(It.IsAny<string>()))
                .Returns<string>(input => input == "test_user");

            var command = new AddLoginDataCommand {
                Username = username
            };
            var violation = Assert.Throws<BusinessRuleViolationException<LoginViolations>>(() => {
                var rule = new AddLoginDataCommandRules(domainService.Object);
                rule.ValidateCommand(command);
            });
            Assert.AreEqual(LoginViolations.InvalidCredentials, violation.ViolatedRule);
        }
    }
}
