﻿using System;
using System.Collections.Generic;

namespace OneAccomplishment.Core.Decorators
{
    public static class HandlerDecorators
    {
        public static IEnumerable<Type> GetCommandHandlerChain()
        {
            return new Type[] {
                typeof(BusinessRulesValidatorCommandHandler<>),
                typeof(TransactionalCommandHandler<>),
                typeof(AuditTrailCommandHandler<>),
                typeof(PermissionsCommandHandler<>),
            };
        }

        public static IEnumerable<Type> GetQueryHandlerChain()
        {
            return new Type[] {
                typeof(PermissionsQueryHandler<,>),
            };
        }
    }
}
