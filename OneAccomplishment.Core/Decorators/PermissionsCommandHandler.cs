﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Decorators
{
    internal sealed class PermissionsCommandHandler<TCommand> 
        : AbstractPermissionsHandler, ICommandHandler<TCommand> 
        where TCommand : class, ICommand
    {
        private readonly ICommandHandler<TCommand> commandHandler;

        public PermissionsCommandHandler(
            ICommandHandler<TCommand> commandHandler,
            IPrincipalService principalService)
            : base(principalService)
        {
            ArgumentGuard.For(() => commandHandler).IsNull().Throw();
            
            this.commandHandler = commandHandler;
        }

        public void Handle(TCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            this.CheckPermissions(typeof(TCommand));
            this.commandHandler.Handle(command);
        }
    }
}
