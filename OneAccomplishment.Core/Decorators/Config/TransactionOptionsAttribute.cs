﻿using System;
using System.Data;

namespace OneAccomplishment.Core.Decorators.Config
{
    /// <summary>
    /// Controls the database transaction behavior for all commands.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class TransactionOptionsAttribute : Attribute
    {
        /// <summary>Gets a value indicating whether this command has disabled transactions</summary>
        /// <value><c>true</c> if this instance has opted out; otherwise, <c>false</c>.</value>
        public bool IsOptOut { get; private set; }
        /// <summary>Gets or sets the database transaction isolation level.</summary>
        public IsolationLevel IsolationLevel { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionOptionsAttribute"/> class.
        /// </summary>
        public TransactionOptionsAttribute()
            : this(false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionOptionsAttribute"/> class.
        /// </summary>
        /// <param name="isOptOut">if set to <c>true</c>, disables transactions for this command.</param>
        public TransactionOptionsAttribute(bool isOptOut)
        {
            this.IsOptOut = isOptOut;
            this.IsolationLevel = IsolationLevel.ReadCommitted;
        }
    }
}
