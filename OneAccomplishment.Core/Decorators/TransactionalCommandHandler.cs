﻿using Fasterflect;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Decorators.Config;

namespace OneAccomplishment.Core.Decorators
{
    internal sealed class TransactionalCommandHandler<TCommand> : ICommandHandler<TCommand> 
        where TCommand : class, ICommand
    {
        private static readonly TransactionOptionsAttribute DefaultTransactionOptions = new TransactionOptionsAttribute();

        private readonly ICommandHandler<TCommand> commandHandler;
        private readonly UnitOfWork unitOfWork;

        public TransactionalCommandHandler(ICommandHandler<TCommand> commandHandler, UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => commandHandler).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();
            
            this.commandHandler = commandHandler;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(TCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            var transactionOptions = typeof(TCommand)
                .Attribute<TransactionOptionsAttribute>() ?? DefaultTransactionOptions;

            if (transactionOptions.IsOptOut) {
                this.commandHandler.Handle(command);
            } else {
                using (var tx = this.unitOfWork.BeginTransaction(transactionOptions.IsolationLevel)) {
                    this.commandHandler.Handle(command);
                    tx.Commit();
                }
            }
        }
    }
}
