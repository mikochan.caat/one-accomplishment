﻿namespace OneAccomplishment.Core.Decorators.Traits
{
    /// <summary>
    /// Marks the command or query for auditing after
    /// execution.
    /// </summary>
    public interface IAuditable
    {
    }
}
