﻿using System.Data;
using OneAccomplishment.Core.Decorators.Config;

namespace OneAccomplishment.Core.Commands.Logins
{
    [TransactionOptions(IsolationLevel = IsolationLevel.Serializable)]
    public sealed class AddLoginDataCommand : ICommand
    {
        public string Username { get; set; }
    }
}
