﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Messages;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class LoginAttemptCommandRules : AbstractLoginCommandRules<LoginAttemptCommand>
    {
        private readonly IClientService clientService;
        private readonly IDomainService domainService;
        private readonly IConfigurationService configService;
        private readonly UnitOfWork unitOfWork;

        public LoginAttemptCommandRules(
            IClientService clientService, 
            IDomainService domainService,
            IConfigurationService configService, 
            UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => clientService).IsNull().Throw();
            ArgumentGuard.For(() => domainService).IsNull().Throw();
            ArgumentGuard.For(() => configService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            this.clientService = clientService;
            this.domainService = domainService;
            this.configService = configService;
            this.unitOfWork = unitOfWork;
        }

        protected override IEnumerable<RuleViolationDelegate> GetRuleChainLinks()
        {
            yield return this.Username_Must_Not_Be_Empty();
            yield return this.Password_Must_Not_Be_Empty();
            yield return this.Allowed_To_Login_Within_Time_Period();
            yield return this.Must_Have_Valid_Domain_Credentials();
        }

        private RuleViolationDelegate Username_Must_Not_Be_Empty()
        {
            return validator => validator.RuleFor(cmd => cmd.Username)
                .NotEmpty()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }

        private RuleViolationDelegate Password_Must_Not_Be_Empty()
        {
            return validator => validator.RuleFor(cmd => cmd.Password)
                .NotEmpty()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }

        private RuleViolationDelegate Allowed_To_Login_Within_Time_Period()
        {
            return validator => validator.RuleFor(cmd => cmd)
                .Must(cmd => {
                    var requestInfo = this.clientService.CurrentRequest;
                    var attempt = this.unitOfWork.LoginAttempts
                        .Where(e => e.Login.Username == cmd.Username &&
                                    e.IpAddress == requestInfo.ClientAddress)
                        .SingleOrDefault();
                    if (attempt == null) {
                        throw new InvalidOperationException(LoginMessages.LoginAttemptNotTracked);
                    }

                    if (attempt.AttemptCount != 0 && 
                        attempt.AttemptCount % this.configService.LoginAttemptMaxLimit == 0) {
                        var duration = (DateTime.UtcNow - attempt.LastAttemptUtc).Duration();
                        return duration.TotalSeconds >= this.configService.LoginAttemptExceededTimeout;
                    }
                    return true;
                })
                .WithViolation(LoginViolations.LoginAttemptExceeded)
                .WithMessageArguments(this.configService.LoginAttemptExceededTimeout);
        }

        private RuleViolationDelegate Must_Have_Valid_Domain_Credentials()
        {
            return validator => validator.RuleFor(cmd => cmd)
                .Must(cmd => this.domainService.ValidateCredentials(cmd.Username, cmd.Password))
                .WithViolation(LoginViolations.InvalidCredentials);
        }
    }
}
