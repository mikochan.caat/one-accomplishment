﻿using System;
using System.Linq;
using LinqToDB;
using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules.Messages;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class LoginAttemptCommandHandler : ICommandHandler<LoginAttemptCommand>
    {
        private readonly IClientService clientService;
        private readonly IAuthenticationService authService;
        private readonly UnitOfWork unitOfWork;

        public LoginAttemptCommandHandler(
            IClientService clientService, IAuthenticationService authService, UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => clientService).IsNull().Throw();
            ArgumentGuard.For(() => authService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            this.clientService = clientService;
            this.authService = authService;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(LoginAttemptCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            var requestInfo = this.clientService.CurrentRequest;
            this.unitOfWork.LoginAttempts
                .Where(e => e.Login.Username == command.Username &&
                            e.IpAddress == requestInfo.ClientAddress)
                .Set(e => e.LastAttemptUtc, DateTime.UtcNow)
                .Set(e => e.AttemptCount, 0)
                .Update();

            long? loginIdRef = this.unitOfWork.Logins
                .Where(e => e.Username == command.Username)
                .Select(e => (long?)e.LoginId)
                .SingleOrDefault();
            if (!loginIdRef.HasValue) {
                throw new InvalidOperationException(LoginMessages.LoginAttemptNotTracked);
            }

            var ticket = new AuthenticationTicket(command.Username) {
                LoginRefId = loginIdRef.Value,
                IsStayLoggedIn = command.IsStayLoggedIn
            };
            this.authService.GrantAuthentication(ticket);
        }
    }
}
