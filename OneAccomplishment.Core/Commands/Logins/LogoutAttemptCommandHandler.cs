﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class LogoutAttemptCommandHandler : ICommandHandler<LogoutAttemptCommand>
    {
        private readonly IAuthenticationService authService;

        public LogoutAttemptCommandHandler(IAuthenticationService authService)
        {
            ArgumentGuard.For(() => authService).IsNull().Throw();

            this.authService = authService;
        }

        public void Handle(LogoutAttemptCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            this.authService.RevokeAuthentication(command.AuthenticationTicket);
        }
    }
}
