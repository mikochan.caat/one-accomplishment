﻿using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal abstract class AbstractLoginCommandRules<T> : AbstractRules<T, LoginViolations>
        where T : ICommand
    {
    }
}
