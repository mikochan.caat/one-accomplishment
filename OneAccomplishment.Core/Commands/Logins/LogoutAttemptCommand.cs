﻿using OneAccomplishment.Core.Decorators.Config;
using OneAccomplishment.Core.Decorators.Traits;
using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Core.Commands.Logins
{
    [AuditTrailOptions("User was logged out.")]
    [TransactionOptions(true)]
    public sealed class LogoutAttemptCommand : ICommand, IAuditable
    {
        public string Reason { get; set; }
        public IAuthenticationTicket AuthenticationTicket { get; set; }
    }
}
