﻿using System;
using System.Linq;
using LinqToDB;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class FailedLoginDataCommandHandler : ICommandHandler<FailedLoginDataCommand>
    {
        private readonly IClientService clientService;
        private readonly UnitOfWork unitOfWork;

        public FailedLoginDataCommandHandler(IClientService clientService, UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => clientService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            this.clientService = clientService;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(FailedLoginDataCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            var requestInfo = this.clientService.CurrentRequest;
            this.unitOfWork.LoginAttempts
                .Where(e => e.Login.Username == command.Username &&
                            e.IpAddress == requestInfo.ClientAddress)
                .Set(e => e.LastAttemptUtc, DateTime.UtcNow)
                .Set(e => e.AttemptCount, e => e.AttemptCount + 1)
                .Update();
        }
    }
}
