﻿using System;
using System.Linq;
using LinqToDB.Data;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class AddLoginDataCommandHandler : ICommandHandler<AddLoginDataCommand>
    {
        private readonly IClientService clientService;
        private readonly ICachingService cacheService;
        private readonly UnitOfWork unitOfWork;

        public AddLoginDataCommandHandler(
            IClientService clientService, ICachingService cacheService, UnitOfWork unitOfWork)
        {
            this.clientService = clientService;
            this.cacheService = cacheService;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(AddLoginDataCommand command)
        {
            var requestInfo = this.clientService.CurrentRequest;
            this.unitOfWork.Logins
                .Merge()
                .Using(new[] {
                    new {
                        Username = command.Username
                    }
                })
                .On((targ, src) => src.Username == targ.Username)
                .InsertWhenNotMatched(e => new LoginEntity {
                    Username = command.Username
                })
                .Merge();

            this.unitOfWork.Logins
                .Where(e => e.Username == command.Username)
                .Select(e => new {
                    e.LoginId,
                    e.Username
                })
                .MergeInto(this.unitOfWork.LoginAttempts)
                .On((targ, src) => src.LoginId == targ.LoginId && targ.IpAddress == requestInfo.ClientAddress)
                .InsertWhenNotMatched(e => new LoginAttemptEntity {
                    LoginId = e.LoginId,
                    IpAddress = requestInfo.ClientAddress,
                    AttemptCount = 0,
                    LastAttemptUtc = DateTime.UtcNow
                })
                .Merge();

            var hashedUserAgent = this.unitOfWork.UserAgents
                .GetHashedUserAgent(this.unitOfWork, requestInfo.UserAgent);
            this.unitOfWork.UserAgents
                .Merge()
                .Using(new[] {
                    new {
                        HashedValue = hashedUserAgent
                    }
                })
                .On((targ, src) => src.HashedValue == targ.HashedValue)
                .InsertWhenNotMatched(e => new UserAgentEntity {
                    UAString = requestInfo.UserAgent
                })
                .Merge();
        }
    }
}
