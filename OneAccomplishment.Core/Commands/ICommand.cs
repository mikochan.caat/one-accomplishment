﻿namespace OneAccomplishment.Core.Commands
{
    /// <summary>
    /// Marker interface for all CQS Commands.
    /// </summary>
    public interface ICommand
    {
    }
}
