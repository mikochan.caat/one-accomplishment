﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    internal static class RuleViolationExtensions
    {
        /// <summary>
        /// Sets the error code, state, and message for this rule with predefined
        /// values based on the type of violation key passed.
        /// </summary>
        /// <typeparam name="T">The type of the validated command.</typeparam>
        /// <typeparam name="TProperty">The type of a property from the validated command.</typeparam>
        /// <typeparam name="TViolation">The type of rule violation.</typeparam>
        /// <param name="rule">The rule.</param>
        /// <param name="violationKey">The violation key.</param>
        /// <returns>A RuleViolationBuilderOptions object for further configuration.</returns>
        public static RuleViolationBuilderOptions<T, TProperty, TViolation> 
            WithViolation<T, TProperty, TViolation>(
            this IRuleBuilderOptions<T, TProperty> rule,
            TViolation violation)
            where T : ICommand
            where TViolation : IRuleViolation<TViolation>
        {
            ArgumentGuard.For(() => rule).IsNull().Throw();

            string violationMessage = violation.AssociatedMessage;
            var associatedRule = rule
                .WithErrorCode(violation.Value)
                .WithState(_ => violation)
                .WithMessage(violationMessage);
            return new RuleViolationBuilderOptions<T, TProperty, TViolation>(
                associatedRule, 
                violation,
                violationMessage);
        }

        public struct RuleViolationBuilderOptions<T, TProperty, TViolation>
            : IRuleViolationBuilderOptions<T, TProperty, TViolation>
            where T : ICommand
            where TViolation : IRuleViolation<TViolation>
        {
            private readonly TViolation violatedRule;
            private readonly string violationMessage;

            private IRuleBuilderOptions<T, TProperty> associatedRule;

            IRuleBuilderOptions<T, TProperty> 
                IRuleViolationBuilderOptions<T, TProperty, TViolation>.AssociatedRule
            {
                get { return this.associatedRule; }
            }

            TViolation IRuleViolationBuilderOptions<T, TProperty, TViolation>.ViolatedRule
            {
                get { return this.violatedRule; }
            }

            public RuleViolationBuilderOptions(
                IRuleBuilderOptions<T, TProperty> associatedRule,
                TViolation violatedRule,
                string violationMessage)
                : this()
            {
                ArgumentGuard.For(() => associatedRule).IsNull().Throw();
                ArgumentGuard.For<object>(() => violatedRule).IsNull().Throw();
                ArgumentGuard.For(() => violationMessage).IsNull().Throw();

                this.associatedRule = associatedRule;
                this.violatedRule = violatedRule;
                this.violationMessage = violationMessage;
            }

            /// <summary>
            /// Formats the configured violation's message with the passed in
            /// message format arguments.
            /// </summary>
            /// <param name="formatArg">The format argument.</param>
            /// <param name="otherFormatArgs">The other format arguments.</param>
            /// <returns>The current RuleViolationBuilderOptions object.</returns>
            public RuleViolationBuilderOptions<T, TProperty, TViolation> 
                WithMessageArguments(object formatArg, params object[] otherFormatArgs)
            {
                IEnumerable<object> args;
                if (otherFormatArgs == null || otherFormatArgs.Length == 0) {
                    args = new[] { formatArg };
                } else {
                    args = (new[] { formatArg }).Concat(otherFormatArgs);
                }
                this.associatedRule = this.associatedRule.WithMessage(
                    this.violationMessage, 
                    args.ToArray());

                return this;
            }
        }
    }
}
