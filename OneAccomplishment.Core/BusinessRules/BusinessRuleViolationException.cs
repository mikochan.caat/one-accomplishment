﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>
    /// Represents a business rule validation failure.
    /// </summary>
    /// <typeparam name="TViolation">The type of rule violation.</typeparam>
    public sealed class BusinessRuleViolationException<TViolation> : Exception 
        where TViolation : IRuleViolation<TViolation>
    {
        private readonly string exceptionMessage;

        /// <summary>Gets the type of the validator used.</summary>
        /// <value>The type of the validator.</value>
        public Type ValidatorType { get; private set; }
        /// <summary>Gets the executed command.</summary>
        /// <value>The executed command.</value>
        public ICommand ExecutedCommand { get; private set; }
        /// <summary>Gets the violated rule.</summary>
        /// <value>The violated rule.</value>
        public TViolation ViolatedRule { get; private set; }
        /// <summary>Gets the validated property of the command.</summary>
        /// <value>The command's validated property.</value>
        public string CommandProperty { get; private set; }

        /// <summary>Gets the user-friendly error code for this violation.</summary>
        /// <value>The error code.</value>
        public string ErrorCode
        {
            get { return this.ViolatedRule.ToString(); }
        }

        /// <summary>
        /// Gets the associated error message for this violation.
        /// </summary>
        public override string Message
        {
            get { return this.exceptionMessage; }
        }

        /// <summary>Initializes a new instance of the <see cref="BusinessRuleViolationException{TViolation}"/> class.</summary>
        /// <param name="validatorType">The type of the validator.</param>
        /// <param name="command">The executed command.</param>
        /// <param name="violation">The violated rule.</param>
        /// <param name="validationFailure">The raw validation error.</param>
        public BusinessRuleViolationException(
            Type validatorType, 
            ICommand command,
            TViolation violation,
            ValidationFailure validationFailure)
            : base(null, CreateValidationException(validatorType, validationFailure))
        {
            ArgumentGuard.For(() => validatorType).IsNull().Throw();
            ArgumentGuard.For(() => command).IsNull().Throw();
            ArgumentGuard.For<object>(() => violation).IsNull().Throw();

            this.ValidatorType = validatorType;
            this.ExecutedCommand = command;
            this.ViolatedRule = violation;
            this.CommandProperty = validationFailure.PropertyName;
            this.exceptionMessage = validationFailure.ErrorMessage;
        }

        private static ValidationException CreateValidationException(
            Type validatorType, ValidationFailure validationFailure)
        {
            ArgumentGuard.For(() => validatorType).IsNull().Throw();
            ArgumentGuard.For(() => validationFailure).IsNull().Throw();
            
            string message = String.Format("[{0}] in '{1}'", 
                validationFailure.ErrorCode, 
                validatorType.FullName);
            return new ValidationException(message, new[] { validationFailure });
        }
    }
}
