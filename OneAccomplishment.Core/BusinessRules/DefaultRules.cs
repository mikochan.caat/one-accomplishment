﻿using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>
    /// The default business rule validator for all commands without specific rules.
    /// </summary>
    /// <typeparam name="T">The command type to validate.</typeparam>
    /// <seealso cref="OneAccomplishment.Core.BusinessRules.AbstractRules{T}" />
    public sealed class DefaultRules<T> : IRules<T> where T : ICommand
    {
        public void ValidateCommand(T command)
        {
        }
    }
}
