﻿using System;
using FluentValidation;
using Net40Utilities.Validation;

namespace OneAccomplishment.Core.BusinessRules
{
    internal static class RuleExtensions
    {
        public static void ChainRules<T>(
            this AbstractValidator<T> validator, params RuleChainLinkDelegate<T>[] ruleChainLinks)
        {
            ArgumentGuard.For(() => validator).IsNull().Throw();
            ArgumentGuard.For(() => ruleChainLinks).IsNull().Throw();
            if (ruleChainLinks.Length == 0) {
                throw new InvalidOperationException("There must be at least 1 rule chain link.");
            }

            Action<AbstractValidator<T>> connector = null;
            for (int i = ruleChainLinks.Length - 1; i >= 0; i--) {
                var current = ruleChainLinks[i];
                var currentConnector = connector;
                if (connector == null) {
                    connector = (val) => current(val);
                } else {
                    connector = (val) => current(val).DependentRules(currentConnector);
                }
            }
            connector(validator);
        }
    }
}
