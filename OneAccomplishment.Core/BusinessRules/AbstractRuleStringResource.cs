﻿using System;
using System.Resources;
using Net40Utilities.Validation;

namespace OneAccomplishment.Core.BusinessRules
{
    public abstract class AbstractRuleStringResource<T> : IRuleStringResource<T>
        where T : AbstractRuleStringResource<T>
    {
        protected readonly string key;
        protected readonly ResourceManager commonResourceManager;
        protected readonly ResourceManager resourceManager;

        /// <summary>Gets the key of this rule string resource.</summary>
        /// <value>The key.</value>
        public string Key
        {
            get { return this.key; }
        }

        /// <summary>
        /// Gets the value of this rule string resource (i.e. the content).
        /// </summary>
        /// <value>The value.</value>
        public string Value
        {
            get
            {
                string value = this.commonResourceManager.GetString(this.key, null);
                if (value == null) {
                    value = this.resourceManager.GetString(this.key, null);
                }
                return value;
            }
        }

        protected AbstractRuleStringResource(
            string key, ResourceManager commonResourceManager, ResourceManager resourceManager)
        {
            ArgumentGuard.For(() => key).IsNull().Throw();
            ArgumentGuard.For(() => commonResourceManager).IsNull().Throw();
            ArgumentGuard.For(() => resourceManager).IsNull().Throw();

            this.key = key;
            this.commonResourceManager = commonResourceManager;
            this.resourceManager = resourceManager;
        }

        /// <summary>
        /// Indicates whether this rule string resource is equal to another instance of the same type.
        /// </summary>
        /// <param name="other">An rule string resource to compare with this instance.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(T other)
        {
            return other != null && this.key.Equals(other.key, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as T);
        }

        public override int GetHashCode()
        {
            return StringComparer.OrdinalIgnoreCase.GetHashCode(this.key);
        }

        /// <summary>Gets the textual representation of this rule string resource.</summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.Value;
        }
    }
}
