﻿using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>
    /// Provides validation of business rules against a command.
    /// </summary>
    /// <typeparam name="T">The command type to validate.</typeparam>
    public interface IRules<T> where T : ICommand
    {
        /// <summary>
        /// Validates a command with a defined set of business rules.
        /// <para>If validation fails, this method will throw a BusinessRuleViolationException of the proper type.</para>
        /// </summary>
        /// <param name="command">The command to validate.</param>
        void ValidateCommand(T command);
    }
}
