﻿using System.Resources;

namespace OneAccomplishment.Core.BusinessRules
{
    public abstract class AbstractRuleViolation<T> : AbstractRuleStringResource<T>, IRuleViolation<T>
        where T : AbstractRuleViolation<T>
    {
        /// <summary>Gets the associated message for this rule violation.</summary>
        /// <value>The associated message.</value>
        public abstract string AssociatedMessage { get; }

        protected AbstractRuleViolation(
            string key, ResourceManager commonResourceManager, ResourceManager resourceManager)
            : base(key, commonResourceManager, resourceManager)
        {
        }
    }
}
