﻿using LinqToDB;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Data
{
    internal static class UserAgentExtensions
    {
        private static readonly CacheOptions CacheOptions = new CacheOptions {
            TypeKey = typeof(UserAgentEntity)
        };

        public static byte[] GetHashedUserAgent(
            this ITable<UserAgentEntity> userAgents, UnitOfWork unitOfWork, string uaString)
        {
            return ((ICachingService)unitOfWork).GetOrAdd(uaString, () => {
                byte[] hashedValue = null;
                unitOfWork.UtilitiesGetHashedString(uaString, ref hashedValue);
                return hashedValue;
            }, CacheOptions);
        }
    }
}
