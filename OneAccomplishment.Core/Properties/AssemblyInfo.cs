﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("One Accomplishment .NET Core Logic Library")]
[assembly: AssemblyDescription("TMS task accomplishments generator")]
[assembly: AssemblyProduct("OneAccomplishment")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("fc0fdf84-5156-43b9-8cd4-b6f0643ad5f5")]

[assembly: AssemblyVersion("1.0.*")]

// Enable internal sharing
[assembly: InternalsVisibleTo("OneAccomplishment.Fakes")]
[assembly: InternalsVisibleTo("OneAccomplishment.UnitTests")]
