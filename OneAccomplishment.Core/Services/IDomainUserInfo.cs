﻿namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Represents the personal information of a domain user. 
    /// </summary>
    public interface IDomainUserInfo
    {
        /// <summary>
        /// The user's domain email address.
        /// </summary>
        string EmailAddress { get; }
        /// <summary>
        /// The user's display name.
        /// </summary>
        string FullName { get; }
        /// <summary>
        /// The user's given name and surname initials.
        /// </summary>
        string Initials { get; }
        /// <summary>
        /// The user's domain ID.
        /// </summary>
        string Username { get; }
    }
}
