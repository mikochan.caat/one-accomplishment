﻿using System;
using System.Linq;
using System.Threading;
using Fasterflect;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Services.Configuration
{
    internal sealed partial class ConfigurationService : IConfigurationService
    {
        private static readonly CacheOptions ConfigCacheOptions = new CacheOptions {
            TypeKey = typeof(ConfigurationService)
        };

        private readonly ICachingService cachingService;
        private readonly Lazy<UnitOfWorkStoredProcedures.ConfigurationsGetValuesResult> configSource;

        public ConfigurationService(ICachingService cachingService, UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => cachingService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            this.cachingService = cachingService;
            this.configSource = this.GetConfigSource(unitOfWork);
        }

        private Lazy<UnitOfWorkStoredProcedures.ConfigurationsGetValuesResult> GetConfigSource(
            UnitOfWork unitOfWork)
        {
            return this.cachingService.GetOrAdd("system", () => {
                return new Lazy<UnitOfWorkStoredProcedures.ConfigurationsGetValuesResult>(
                    this.GetSystemConfig(unitOfWork),
                    LazyThreadSafetyMode.ExecutionAndPublication);
            }, ConfigCacheOptions);
        }

        private Func<UnitOfWorkStoredProcedures.ConfigurationsGetValuesResult> 
            GetSystemConfig(UnitOfWork unitOfWork)
        {
            return () => {
                var config = unitOfWork.ConfigurationsGetValues().SingleOrDefault();
                if (config == null) {
                    throw new SystemConfigurationException();
                }

                // Check for null values, which is not allowed
                var properties = config.GetType().PropertiesWith(Flags.Public | Flags.Instance);
                foreach (var prop in properties) {
                    if (prop.Get(config) == null) {
                        throw new SystemConfigurationException(prop.Name);
                    }
                }

                return config;
            };
        }
    }
}
