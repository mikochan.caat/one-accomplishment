﻿namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract API for domain-specific services.
    /// </summary>
    public interface IDomainService : ICoreService
    {
        /// <summary>
        /// Checks if the specified user exists in the domain.
        /// </summary>
        /// <param name="username">Username to check.</param>
        /// <returns>true if the username exists, otherwise false.</returns>
        bool IsUsernameExisting(string username);
        /// <summary>
        /// Validates the credentials against the domain.
        /// </summary>
        /// <param name="username">Username to validate.</param>
        /// <param name="password">Password to validate.</param>
        /// <returns>true if successful, otherwise false.</returns>
        bool ValidateCredentials(string username, string password);
        /// <summary>
        /// Gets the user's domain information.
        /// </summary>
        /// <param name="username">Username to retrieve.</param>
        /// <returns>The user's domain information.</returns>
        IDomainUserInfo GetDomainUserInfo(string username);
    }
}
