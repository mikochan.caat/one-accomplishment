﻿namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract API for retrieving system configurations.
    /// </summary>
    public partial interface IConfigurationService : ICoreService
    {
    }
}
