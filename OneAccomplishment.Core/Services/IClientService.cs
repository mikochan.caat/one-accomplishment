﻿namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract API for getting client-specific information.
    /// </summary>
    public interface IClientService
    {
        /// <summary>
        /// The client's request information for the current scope.
        /// </summary>
        IRequestInfo CurrentRequest { get; }
    }
}
