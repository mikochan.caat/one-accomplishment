﻿namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Represents the current client's information
    /// for the current request.
    /// </summary>
    public interface IRequestInfo
    {
        /// <summary>
        /// The client's origin IP Address.
        /// </summary>
        string ClientAddress { get; }
        /// <summary>
        /// The client's User-Agent.
        /// </summary>
        string UserAgent { get; }
    }
}
