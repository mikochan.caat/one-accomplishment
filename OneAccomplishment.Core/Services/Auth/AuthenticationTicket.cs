﻿namespace OneAccomplishment.Core.Services.Auth
{
    public sealed class AuthenticationTicket : IAuthenticationTicket
    {
        public string Username { get; private set; }
        public long LoginRefId { get; set; }
        public bool IsStayLoggedIn { get; set; }

        public bool IsValidTicket
        {
            get { return true; }
        }

        public AuthenticationTicket(string username)
        {
            this.Username = username;
        }
    }
}
