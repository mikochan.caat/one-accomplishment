﻿using System;
using Net40Utilities.Validation;

namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Provides a permission checking filter.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, 
        AllowMultiple = false, 
        Inherited = false)]
    public sealed class PermissionFilterAttribute : Attribute
    {
        /// <summary>Gets the permission operation to use.</summary>
        /// <value>The permission operation.</value>
        public PermissionOperation PermissionOperation { get; private set; }
        /// <summary>Gets the required permissions for this filter.</summary>
        /// <value>The permissions.</value>
        public PermissionSet Permissions { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionFilterAttribute"/> class.
        /// <para>This implies a PermissionOperation.And operation.</para>
        /// </summary>
        /// <param name="permissionOperation">The permission operation.</param>
        /// <param name="permissions">The permissions that are required.</param>
        public PermissionFilterAttribute(params long[] permissions)
            : this(PermissionOperation.And, permissions)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="PermissionFilterAttribute"/> class.</summary>
        /// <param name="permissionOperation">The permission operation.</param>
        /// <param name="permissions">The permissions that are required.</param>
        public PermissionFilterAttribute(PermissionOperation permissionOperation, params long[] permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            this.PermissionOperation = permissionOperation;
            this.Permissions = new PermissionSet(permissions);
        }

        /// <summary>Demands the permissions of this filter against the reference PermissionSet.</summary>
        /// <param name="permissionsReference">The permissions reference.</param>
        /// <returns><c>true</c> if the permissions are available using the configured PermissionOperation; otherwise, <c>false</c>.</returns>
        public bool Demand(PermissionSet permissionsReference)
        {
            ArgumentGuard.For(() => permissionsReference).IsNull().Throw();

            if (this.PermissionOperation == PermissionOperation.And) {
                return permissionsReference.HasAllPermissions(this.Permissions);
            }
            return permissionsReference.HasAnyPermissions(this.Permissions);
        }
    }
}
