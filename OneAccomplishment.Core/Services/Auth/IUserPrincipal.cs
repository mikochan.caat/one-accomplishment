﻿using System.Collections.Generic;

namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Represents an aggregate of a user's
    /// login and domain information, and permissions.
    /// </summary>
    public interface IUserPrincipal
    {
        /// <summary>Gets the user's domain ID.</summary>
        /// <value>The user's username.</value>
        string Username { get; }
        /// <summary>Gets the user's internal ID.</summary>
        /// <value>The user's login reference identifier.</value>
        long LoginRefId { get; }
        /// <summary>Gets the user's domain information.</summary>
        /// <value>The user's domain information.</value>
        IDomainUserInfo DomainInfo { get; }
        /// <summary>Gets the granted permissions for this user.</summary>
        /// <value>The user's granted permissions.</value>
        PermissionSet Permissions { get; }
        /// <summary>Gets the roles this user belongs to.</summary>
        /// <value>The user's roles.</value>
        IList<string> Roles { get; }
    }
}
