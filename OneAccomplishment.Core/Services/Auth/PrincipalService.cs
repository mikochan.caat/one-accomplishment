﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using LinqToDB;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Site.Core.Services
{
    internal sealed class PrincipalService : IPrincipalService
    {
        private static readonly CacheOptions DefaultCacheOptions = new CacheOptions {
            TypeKey = typeof(PrincipalService)
        };

        private readonly IAuthenticationService authService;
        private readonly IDomainService domainService;
        private readonly ICachingService cacheService;
        private readonly Lazy<IUserPrincipal> currentPrincipal;
        private readonly UnitOfWork unitOfWork;

        IUserPrincipal IPrincipalService.CurrentPrincipal
        {
            get { return this.currentPrincipal.Value; }
        }

        public PrincipalService(
            IAuthenticationService authService,
            IDomainService domainService,
            ICachingService cacheService,
            UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => authService).IsNull().Throw();
            ArgumentGuard.For(() => domainService).IsNull().Throw();
            ArgumentGuard.For(() => cacheService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            this.authService = authService;
            this.domainService = domainService;
            this.cacheService = cacheService;
            this.unitOfWork = unitOfWork;
            this.currentPrincipal = new Lazy<IUserPrincipal>(
                this.GetCurrentPrincipal,
                LazyThreadSafetyMode.ExecutionAndPublication);

            authService.OnAuthenticationRevoked += AuthService_OnAuthenticationRevoked;
        }

        private void AuthService_OnAuthenticationRevoked(IAuthenticationTicket revokedTicket)
        {
            this.cacheService.Remove(revokedTicket.Username, DefaultCacheOptions);
        }

        private IUserPrincipal GetCurrentPrincipal()
        {
            var ticket = this.authService.CurrentTicket;
            var principalInfo = this.cacheService.GetOrAdd(
                ticket.Username,
                this.CreateUserPrincipalInformation(ticket),
                DefaultCacheOptions);
            return new UserPrincipal {
                Username = ticket.Username,
                LoginRefId = ticket.LoginRefId,
                DomainInfo = this.domainService.GetDomainUserInfo(ticket.Username),
                Permissions = principalInfo.Permissions,
                Roles = principalInfo.Roles
            };
        }

        private Func<PrincipalInfo> CreateUserPrincipalInformation(IAuthenticationTicket ticket)
        {
            return () => {
                var roleNames =
                    from lrm in this.unitOfWork.LoginRoleMappings.InnerJoin(e => e.RoleId == e.Role.RoleId)
                    where lrm.LoginId == ticket.LoginRefId
                    orderby lrm.RoleId
                    select lrm.Role.RoleName;
                var rolePermissions =
                    from lrm in this.unitOfWork.LoginRoleMappings
                    from rm in this.unitOfWork.RoleMappings.InnerJoin(e => e.RoleId == lrm.RoleId)
                    where lrm.LoginId == ticket.LoginRefId
                    select new {
                        rm.PermissionId,
                        IsGranted = true
                    };
                var userPermissions = 
                    from lpm in this.unitOfWork.LoginPermissionMappings
                    where lpm.LoginId == ticket.LoginRefId
                    select new {
                        lpm.PermissionId,
                        lpm.IsGranted
                    };
                var unionedPermissions = rolePermissions
                    .Concat(userPermissions)
                    .AsEnumerable()
                    .ToLookup(permission => permission.IsGranted, permission => permission.PermissionId);

                var grantedPermissions = new HashSet<long>(unionedPermissions[true]);
                var deniedPermissions = new HashSet<long>(unionedPermissions[false]);
                grantedPermissions.ExceptWith(deniedPermissions);

                return new PrincipalInfo {
                    Permissions = new PermissionSet(grantedPermissions),
                    Roles = roleNames.ToList().AsReadOnly()
                };
            };
        }

        private sealed class UserPrincipal : IUserPrincipal
        {
            public string Username { get; set; }
            public long LoginRefId { get; set; }
            public IDomainUserInfo DomainInfo { get; set; }
            public PermissionSet Permissions { get; set; }
            public IList<string> Roles { get; set; }
        }

        private sealed class PrincipalInfo
        {
            public PermissionSet Permissions { get; set; }
            public IList<string> Roles { get; set; }
        }
    }
}
