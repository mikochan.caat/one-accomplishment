﻿using System.Collections.Generic;
using System.Linq;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract SQL IQueryable cache.
    /// </summary>
    public interface IDbCachingService
    {
        /// <summary>
        /// Retrieves the cached results for the provided query.
        /// </summary>
        /// <typeparam name="T">Query return type.</typeparam>
        /// <param name="query">The query to retrieve results from and to cache.</param>
        /// <param name="scope">The query's cache scope.</param>
        /// <param name="options">Additional query caching options.</param>
        /// <returns>The cached results of the provided query.</returns>
        IEnumerable<T> FromCache<T>(IQueryable<T> query, DbCacheScope scope, DbCacheOptions options = null);
    }
}
