﻿namespace OneAccomplishment.Core.Services.Caching
{
    /// <summary>
    /// Represents the additional caching options for
    /// the Database Caching Service API.
    /// </summary>
    /// <seealso cref="OneAccomplishment.Core.Services.Caching.CacheOptions" />
    public sealed class DbCacheOptions : CacheOptions
    {
        /// <summary>Gets or sets the supplemental key suffix.</summary>
        /// <value>The key suffix.</value>
        public string KeySuffix { get; set; }
    }
}
