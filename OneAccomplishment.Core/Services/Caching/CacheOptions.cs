﻿using System;

namespace OneAccomplishment.Core.Services.Caching
{
    /// <summary>
    /// Represents the additional caching options for
    /// the Caching Service API.
    /// </summary>
    public class CacheOptions
    {
        /// <summary>Gets or sets the expiration in time units.</summary>
        /// <value>The expiration.</value>
        public TimeSpan? Expiration { get; set; }
        /// <summary>Gets or sets the type of the cache entry expiration.</summary>
        /// <value>The type of the expiration.</value>
        public CacheExpiry ExpirationType { get; set; }
        /// <summary>Gets or sets the Type key for additional cache key segregation.</summary>
        /// <value>The Type key.</value>
        public Type TypeKey { get; set; }

        public CacheOptions()
        {
            this.ExpirationType = CacheExpiry.Absolute;
        }
    }
}
