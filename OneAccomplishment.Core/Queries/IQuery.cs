﻿namespace OneAccomplishment.Core.Queries
{
    /// <summary>
    /// Marker interface for all CQS Queries.
    /// </summary>
    /// <typeparam name="TResult">Query return type.</typeparam>
    public interface IQuery<TResult>
    {
    }
}
