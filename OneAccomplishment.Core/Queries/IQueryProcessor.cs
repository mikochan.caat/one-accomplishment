﻿namespace OneAccomplishment.Core.Queries
{
    /// <summary>
    /// Provides automatic delegation of queries
    /// to query handlers.
    /// </summary>
    public interface IQueryProcessor
    {
        /// <summary>
        /// Delegates a query to its proper query handler.
        /// </summary>
        /// <typeparam name="TResult">Query return type.</typeparam>
        /// <param name="query">Query to handle.</param>
        /// <returns>An instance of an object as indicated in the query's return type.</returns>
        TResult Process<TResult>(IQuery<TResult> query);
    }
}
