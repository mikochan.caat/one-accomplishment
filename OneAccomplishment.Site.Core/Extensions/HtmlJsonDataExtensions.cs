﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;
using Newtonsoft.Json;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class HtmlJsonDataExtensions
    {
        public static IHtmlString ObjectAsDataAttribute(this HtmlHelper html, object obj, string dataId = null)
        {
            ArgumentGuard.For(() => html).IsNull().Throw();

            var dataBuilder = new StringBuilder("data-json", 1024);
            if (!String.IsNullOrWhiteSpace(dataId)) {
                dataBuilder.AppendFormat("-{0}", dataId);
            }
            string jsonData = JsonConvert.SerializeObject(obj);
            string jsonEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonData));
            dataBuilder.AppendFormat("=\"{0}\"", jsonEncoded);

            return new MvcHtmlString(dataBuilder.ToString());
        }
    }

    public static class HtmlDataIds
    {
        public const string MainViewModel = "main-vm";
    }
}
