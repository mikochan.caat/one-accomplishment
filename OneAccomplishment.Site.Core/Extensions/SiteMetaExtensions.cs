﻿using System;
using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;
using OneAccomplishment.Site.Core.Mvc;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class SiteMetaExtensions
    {
        private static readonly Type SiteMetadataItemKey = typeof(SiteMetaExtensions);

        public static IHtmlString RenderMainScript(this HtmlHelper html, string mainScriptModuleName)
        {
            ArgumentGuard.For(() => html).IsNull().Throw();
            ArgumentGuard.For(() => mainScriptModuleName).IsNullOrWhiteSpace().Throw();

            return new MvcHtmlString(String.Format("data-main-script=\"{0}\"", mainScriptModuleName));
        }

        public static void SetSiteMetadata(this WebViewPage page, SiteMetadata metadata)
        {
            ArgumentGuard.For(() => page).IsNull().Throw();
            ArgumentGuard.For(() => metadata).IsNull().Throw();

            var items = page.Context.Items;
            var existingItem = items[SiteMetadataItemKey];
            if (existingItem != null && !(existingItem is SiteMetadata)) {
                throw new InvalidOperationException(
                    "SiteMetadata key is reserved. It should only contain an instance of SiteMetadata.");
            }

            page.Context.Items[SiteMetadataItemKey] = metadata;
        }

        public static SiteMetadata GetSiteMetadata(this WebViewPage page)
        {
            ArgumentGuard.For(() => page).IsNull().Throw();
            
            var items = page.Context.Items;
            var existingItem = items[SiteMetadataItemKey] as SiteMetadata;
            if (existingItem == null) {
                throw new InvalidOperationException("Current site metadata is invalid or not set.");
            }

            return existingItem;
        }
    }
}
