﻿using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class HtmlValidatorExtensions
    {
        public static IHtmlString PostValidationSummary(this HtmlHelper html, string title = null, object htmlAttributes = null)
        {
            ArgumentGuard.For(() => html).IsNull().Throw();

            var modelState = html.ViewData.ModelState;
            var baseTagBuilder = new TagBuilder("div");
            if (htmlAttributes != null) {
                var attribs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
                baseTagBuilder.MergeAttributes(attribs);
            }
            if (modelState.IsValid) {
                baseTagBuilder.AddCssClass("validation-summary-valid");
            } else {
                baseTagBuilder.AddCssClass("validation-summary-errors");
                if (title != null) {
                    var titleTagBuilder = new TagBuilder("span");
                    titleTagBuilder.SetInnerText(title);
                    baseTagBuilder.InnerHtml += titleTagBuilder.ToString();
                }
                var ulTagBuilder = new TagBuilder("ul");
                foreach (var state in modelState.Values) {
                    foreach (var error in state.Errors) {
                        var liTagBuilder = new TagBuilder("li");
                        liTagBuilder.SetInnerText(error.ErrorMessage);
                        ulTagBuilder.InnerHtml += liTagBuilder.ToString();
                    }
                }
                baseTagBuilder.InnerHtml += ulTagBuilder.ToString();
            }

            return new MvcHtmlString(baseTagBuilder.ToString());
        }
    }
}
