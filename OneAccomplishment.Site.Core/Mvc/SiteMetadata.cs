﻿namespace OneAccomplishment.Site.Core.Mvc
{
    /// <summary>
    /// Contains important information of the current web page.
    /// </summary>
    public sealed class SiteMetadata
    {
        /// <summary>
        /// The title of the current page.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The parent class that is used to determine
        /// which LESS styling will be applied.
        /// </summary>
        public string MainStyle { get; set; }
        /// <summary>
        /// Toggles the visibility of navigation
        /// elements in the layout page.
        /// </summary>
        public bool ShowNavigationElements { get; set; }
        /// <summary>
        /// Sets the layout type of the Layout view.
        /// </summary>
        public LayoutType Layout { get; set; }

        public SiteMetadata()
        {
            this.ShowNavigationElements = true;
            this.Layout = LayoutType.Normal;
        }
    }
}
