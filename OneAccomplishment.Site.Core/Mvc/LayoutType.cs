﻿namespace OneAccomplishment.Site.Core.Mvc
{
    public enum LayoutType
    {
        /// <summary>
        /// Uses the default two-column body layout.
        /// </summary>
        Normal,
        /// <summary>
        /// Uses the single-column body layout with a large header and footer.
        /// </summary>
        SingleLayout
    }
}
