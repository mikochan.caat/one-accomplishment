﻿using System;

namespace OneAccomplishment.Site.Core.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple= false, Inherited = false)]
    public sealed class DisableAnonymousCacheAttribute : Attribute
    {
    }
}
