﻿using System;

namespace OneAccomplishment.Site.Core.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public sealed class NoReturnUrlAttribute : Attribute
    {
    }
}
