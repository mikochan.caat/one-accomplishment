﻿using System;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Queries;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class QueryProcessor : IQueryProcessor
    {
        private readonly Container container;

        public QueryProcessor(Container container)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();

            this.container = container;
        }

        TResult IQueryProcessor.Process<TResult>(IQuery<TResult> query)
        {
            throw new NotImplementedException();
        }
    }
}
