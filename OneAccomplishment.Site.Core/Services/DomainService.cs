﻿using System;
using System.Collections.Concurrent;
using System.DirectoryServices.AccountManagement;
using System.Threading;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class DomainService : IDomainService, IDisposable
    {
        private static readonly CacheOptions DefaultCacheOptions = new CacheOptions {
            TypeKey = typeof(DomainService)
        };

        private readonly Container container;
        private readonly ICachingService cacheService;

        private readonly Lazy<PrincipalContext> context;
        private readonly ConcurrentDictionary<string, UserPrincipal> principalCache;

        public DomainService(Container container, ICachingService cacheService)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();
            ArgumentGuard.For(() => cacheService).IsNull().Throw();

            this.container = container;
            this.cacheService = cacheService;

            this.context = new Lazy<PrincipalContext>(() => {
                return new PrincipalContext(ContextType.Domain);
            }, LazyThreadSafetyMode.ExecutionAndPublication);
            this.principalCache = new ConcurrentDictionary<string, UserPrincipal>();
        }

        bool IDomainService.IsUsernameExisting(string username)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();

            return this.GetUserPrincipal(username) != null;
        }

        bool IDomainService.ValidateCredentials(string username, string password)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();
            ArgumentGuard.For(() => password).IsNull().Throw();

            return this.context.Value.ValidateCredentials(username, password);
        }

        IDomainUserInfo IDomainService.GetDomainUserInfo(string username)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();

            return this.cacheService.GetOrAdd(username, () => {
                var principal = this.GetUserPrincipal(username);
                var userInfo = new DomainUserInfo {
                    Username = username
                };
                if (principal != null) {
                    userInfo.EmailAddress = principal.EmailAddress;
                    userInfo.FullName = principal.DisplayName;
                    userInfo.Initials =
                        this.GetUserNameInitial(principal.GivenName) +
                        this.GetUserNameInitial(principal.Surname);
                }
                return userInfo;
            }, DefaultCacheOptions);
        }

        void IDisposable.Dispose()
        {
            foreach (var principal in this.principalCache.Values) {
                if (principal != null) {
                    principal.Dispose();
                }
            }
            this.principalCache.Clear();
            if (this.context.IsValueCreated) {
                this.context.Value.Dispose();
            }
        }

        private UserPrincipal GetUserPrincipal(string username)
        {
            return this.principalCache.GetOrAdd(username, name => {
                return UserPrincipal.FindByIdentity(
                    this.context.Value,
                    IdentityType.SamAccountName,
                    name);
            });
        }

        private string GetUserNameInitial(string name)
        {
            if (name == null || name.Length == 0) {
                return String.Empty;
            }
            return name[0].ToString();
        }

        private sealed class DomainUserInfo : IDomainUserInfo
        {
            public string EmailAddress { get; set; }
            public string FullName { get; set; }
            public string Initials { get; set; }
            public string Username { get; set; }

            public DomainUserInfo()
            {
                this.EmailAddress = String.Empty;
                this.FullName = String.Empty;
                this.Initials = String.Empty;
            }
        }
    }
}
