﻿# OneAccomplishment.Core.Service Implementations Folder

All implementations here are services that depend on web technology and/or external services which makes putting them in the OneAccomplishment.Core.Service assembly undesirable.
