﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.Services;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class BusinessRulesService : IBusinessRulesService
    {
        private readonly Container container;

        public BusinessRulesService(Container container)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();
            this.container = container;
        }

        IRules<T> IBusinessRulesService.GetRulesFor<T>()
        {
            return this.container.GetInstance<IRules<T>>();
        }
    }
}
