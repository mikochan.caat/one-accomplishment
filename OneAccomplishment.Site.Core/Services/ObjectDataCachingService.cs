﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using LazyCache;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;
using OneAccomplishment.Site.Core.Configuration;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class ObjectDataCachingService : ICachingService, IDbCachingService, IDisposable
    {
        private static readonly CacheOptions DefaultOptions = new CacheOptions();
        private static readonly DbCacheOptions DefaultDbOptions = new DbCacheOptions();

        private readonly Container container;
        private readonly IAppCache globalCache;
        private readonly TimeSpan globalCacheDuration;
        private readonly Lazy<ScopedCache> scopedCache;

        public ObjectDataCachingService(Container container)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();

            this.container = container;
            this.globalCache = new CachingService(new MemoryCache(typeof(ObjectDataCachingService).FullName));
            this.globalCacheDuration = SiteConfiguration.Caching.GlobalDuration;
            this.scopedCache = new Lazy<ScopedCache>(LazyThreadSafetyMode.ExecutionAndPublication);
        }

        T ICachingService.Get<T>(string key, CacheOptions options)
        {
            ArgumentGuard.For(() => key).IsNull().Throw();

            options = options ?? DefaultOptions;
            string internalKey = this.GetGlobalCacheKey(key, options.TypeKey);
            return this.globalCache.Get<T>(internalKey);
        }

        T ICachingService.GetOrAdd<T>(string key, Func<T> factory, CacheOptions options)
        {
            ArgumentGuard.For(() => key).IsNull().Throw();
            ArgumentGuard.For(() => factory).IsNull().Throw();

            options = options ?? DefaultOptions;
            string internalKey = this.GetGlobalCacheKey(key, options.TypeKey);
            var absoluteExpiration = this.GetDefaultAbsoluteExpiration();
            if (options.Expiration.HasValue) {
                if (options.ExpirationType == CacheExpiry.Sliding) {
                    return this.globalCache.GetOrAdd(internalKey, factory, options.Expiration.Value);
                }
                absoluteExpiration = this.GetAbsoluteExpiration(options.Expiration.Value);
            }
            return this.globalCache.GetOrAdd(internalKey, factory, absoluteExpiration);
        }

        void ICachingService.Add<T>(string key, T item, CacheOptions options)
        {
            ArgumentGuard.For(() => key).IsNull().Throw();

            options = options ?? DefaultOptions;
            string internalKey = this.GetGlobalCacheKey(key, options.TypeKey);
            var absoluteExpiration = this.GetDefaultAbsoluteExpiration();
            if (options.Expiration.HasValue) {
                if (options.ExpirationType == CacheExpiry.Sliding) {
                    this.globalCache.Add(internalKey, item, options.Expiration.Value);
                    return;
                }
                absoluteExpiration = this.GetAbsoluteExpiration(options.Expiration.Value);
            }
            this.globalCache.Add(internalKey, item, absoluteExpiration);
        }

        void ICachingService.Remove(string key, CacheOptions options)
        {
            ArgumentGuard.For(() => key).IsNull().Throw();

            options = options ?? DefaultOptions;
            this.globalCache.Remove(this.GetGlobalCacheKey(key, options.TypeKey));
        }

        IEnumerable<T> IDbCachingService.FromCache<T>(
            IQueryable<T> query, DbCacheScope scope, DbCacheOptions options)
        {
            string typeKeyPart = null;
            string keySuffixPart = null;
            options = options ?? DefaultDbOptions;

            if (options.TypeKey != null) {
                typeKeyPart = options.TypeKey.AssemblyQualifiedName;
            }
            if (options.KeySuffix != null) {
                keySuffixPart = (typeKeyPart == null ? null : "_") + options.KeySuffix;
            }

            string compositeKeySuffix = typeKeyPart + keySuffixPart;
            if (scope == DbCacheScope.Global) {
                return this.FromGlobalDbCache(query, compositeKeySuffix, options);
            }
            return this.FromScopedDbCache(query, compositeKeySuffix);
        }

        private IEnumerable<T> FromGlobalDbCache<T>(
            IQueryable<T> query, string keySuffix, DbCacheOptions options)
        {
            ArgumentGuard.For(() => query).IsNull().Throw();

            string key = "datacache:" + this.GetQueryHash(query, keySuffix);
            Func<IEnumerable<T>> factory = () => query.ToArray();

            var absoluteExpiration = this.GetDefaultAbsoluteExpiration();
            if (options.Expiration.HasValue) {
                if (options.ExpirationType == CacheExpiry.Sliding) {
                    return this.globalCache.GetOrAdd(key, factory, options.Expiration.Value);
                }
                absoluteExpiration = this.GetAbsoluteExpiration(options.Expiration.Value);
            }
            return this.globalCache.GetOrAdd(key, factory, absoluteExpiration);
        }

        private IEnumerable<T> FromScopedDbCache<T>(IQueryable<T> query, string keySuffix)
        {
            ArgumentGuard.For(() => query).IsNull().Throw();

            string key = this.GetQueryHash(query, keySuffix);
            return (IEnumerable<T>)this.scopedCache.Value.GetOrAdd(key, _ => query.ToArray());
        }

        private string GetGlobalCacheKey(string key, Type typeKey)
        {
            string typeKeyString = typeKey == null ? null : typeKey.AssemblyQualifiedName + "_";
            return "globalcache:" + typeKeyString + key;
        }

        private DateTimeOffset GetDefaultAbsoluteExpiration()
        {
            return DateTimeOffset.UtcNow.Add(this.globalCacheDuration);
        }

        private DateTimeOffset GetAbsoluteExpiration(TimeSpan time)
        {
            return DateTimeOffset.UtcNow.Add(time);
        }

        private string GetQueryHash<T>(IQueryable<T> query, string keySuffix)
        {
            using (var sha = SHA512.Create()) {
                var hashBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(query.ToString()));
                return BitConverter.ToString(hashBytes) + keySuffix;
            }
        }

        void IDisposable.Dispose()
        {
            if (this.scopedCache.IsValueCreated) {
                ((IDisposable)this.scopedCache.Value).Dispose();
            }
        }

        private sealed class ScopedCache : ConcurrentDictionary<string, IEnumerable>, IDisposable
        {
            public ScopedCache()
                : base(StringComparer.OrdinalIgnoreCase)
            {
            }

            void IDisposable.Dispose()
            {
                this.Clear();
            }
        }
    }
}
