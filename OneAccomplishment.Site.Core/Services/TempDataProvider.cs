﻿/**
 * Implementation inspired by: https://github.com/mwijnands/CacheTempData
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;
using OneAccomplishment.Site.Core.Configuration;
using OneAccomplishment.Site.Core.Extensions;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class TempDataProvider : ITempDataProvider
    {
        private static readonly Type TempDataCorrelationItemKey = TempDataCorrelationExtensions.TypeItemKey;

        private readonly Container container;
        private readonly ICachingService cacheService;
        private readonly CacheOptions cacheOptions;
        private readonly object cookieLock;

        public TempDataProvider(Container container, ICachingService cacheService)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();
            ArgumentGuard.For(() => cacheService).IsNull().Throw();

            this.container = container;
            this.cacheService = cacheService;
            this.cookieLock = new object();

            this.cacheOptions = new CacheOptions {
                Expiration = SiteConfiguration.Caching.TempDataDuration,
                ExpirationType = CacheExpiry.Sliding,
                TypeKey = this.GetType()
            };
        }

        IDictionary<string, object> ITempDataProvider.LoadTempData(ControllerContext controllerContext)
        {
            ArgumentGuard.For(() => controllerContext).IsNull().Throw();

            string cacheKey = this.GetCompositeCacheKey(controllerContext.HttpContext);
            var tempData = this.cacheService.Get<IDictionary<string, object>>(cacheKey, this.cacheOptions);
            this.cacheService.Remove(cacheKey, this.cacheOptions);
            if (tempData != null) {
                return tempData;
            }
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        }

        void ITempDataProvider.SaveTempData(
            ControllerContext controllerContext, IDictionary<string, object> values)
        {
            ArgumentGuard.For(() => controllerContext).IsNull().Throw();

            var httpItems = controllerContext.HttpContext.Items;
            string correlationId = httpItems[TempDataCorrelationItemKey] as string;
            if (correlationId == null) {
                return;
            }
            if (String.IsNullOrWhiteSpace(correlationId)) {
                throw new InvalidOperationException("The TempData correlation HTTP item was invalid.");
            }

            string cacheKey = this.GetCompositeCacheKey(controllerContext.HttpContext, correlationId);
            this.cacheService.Remove(cacheKey, this.cacheOptions);
            if (values != null && values.Count > 0) {
                this.cacheService.Add(cacheKey, values, this.cacheOptions);
            }
        }

        private string GetCompositeCacheKey(HttpContextBase context, string correlationId = null)
        {
            string cookieId = this.IssueAndRetrieveTempDataCookieId(context);
            if (correlationId == null) {
                var configService = this.container.GetInstance<IConfigurationService>();
                string correlationParam = configService.TempDataCorrelationParamName;
                correlationId = context.Request.QueryString[correlationParam];
            }
            if (String.IsNullOrWhiteSpace(correlationId)) {
                correlationId = null;
            } else {
                correlationId = "_" + correlationId;
            }
            return cookieId + correlationId;
        }

        private string IssueAndRetrieveTempDataCookieId(HttpContextBase context)
        {
            var configService = this.container.GetInstance<IConfigurationService>();
            string tempDataCookieName = configService.TempDataCorrelationCookieName;
            string responseCookieValue;
            if (this.TryGetResponseTempDataCookieValue(
                context, tempDataCookieName, out responseCookieValue)) {
                return responseCookieValue;
            }

            var requestCookies = context.Request.Cookies;
            var requestTempDataCookie = requestCookies.Get(tempDataCookieName);
            if (requestTempDataCookie != null) {
                return requestTempDataCookie.Value;
            }

            lock (this.cookieLock) {
                if (this.TryGetResponseTempDataCookieValue(
                    context, tempDataCookieName, out responseCookieValue)) {
                    return responseCookieValue;
                }

                string cookieValue = Guid.NewGuid().ToString();
                var valueBytes = Encoding.UTF8.GetBytes(cookieValue);
                string encryptedValue = MachineKey.Encode(valueBytes, MachineKeyProtection.All);
                var responseTempDataCookie = new HttpCookie(tempDataCookieName, encryptedValue) {
                    HttpOnly = true,
                    Path = context.Request.ApplicationPath,
                    Secure = context.Request.IsSecureConnection
                };

                context.Response.Cookies.Add(responseTempDataCookie);
                return cookieValue;
            }
        }

        private bool TryGetResponseTempDataCookieValue(
            HttpContextBase context, string tempDataCookieName, out string cookieValue)
        {
            cookieValue = null;
            var responseCookies = context.Response.Cookies;
            bool hasCookie = responseCookies.AllKeys.Contains(tempDataCookieName);
            if (hasCookie) {
                var cookie = responseCookies.Get(tempDataCookieName);
                var decodedValue = MachineKey.Decode(cookie.Value, MachineKeyProtection.All);
                cookieValue = Encoding.UTF8.GetString(decodedValue);
            }
            return hasCookie;
        }
    }
}
