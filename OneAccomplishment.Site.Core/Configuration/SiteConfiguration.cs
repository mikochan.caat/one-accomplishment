﻿using System.Configuration;

namespace OneAccomplishment.Site.Core.Configuration
{
    /// <summary>
    /// Provides the website infrastructure configuration values.
    /// </summary>
    public static class SiteConfiguration
    {
        /// <summary>Gets the caching configuration.</summary>
        /// <value>The caching configuration.</value>
        public static ICachingConfig Caching
        {
            get { return GetSiteSection<ICachingConfig>("caching"); }
        }

        /// <summary>Gets the HTTP compression configuration.</summary>
        /// <value>The HTTP compression configuration.</value>
        public static IHttpCompressionConfig HttpCompression
        {
            get { return GetSiteSection<IHttpCompressionConfig>("httpCompression"); }
        }

        private static T GetSiteSection<T>(string sectionName)
        {
            return (T)ConfigurationManager.GetSection("siteSettings/" + sectionName);
        }
    }
}
