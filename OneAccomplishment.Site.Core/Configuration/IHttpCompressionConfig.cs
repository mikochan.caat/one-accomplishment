﻿using System.Collections.Generic;

namespace OneAccomplishment.Site.Core.Configuration
{
    /// <summary>
    /// Provides the configuration values for HTTP compression.
    /// </summary>
    public interface IHttpCompressionConfig
    {
        /// <summary>Gets the supported HTTP methods for compression.</summary>
        /// <value>The supported HTTP methods.</value>
        ISet<string> SupportedMethods { get; }
    }
}
