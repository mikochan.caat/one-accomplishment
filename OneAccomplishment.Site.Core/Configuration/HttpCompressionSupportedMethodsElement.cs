﻿using System.Configuration;

namespace OneAccomplishment.Site.Core.Configuration
{
    internal partial class HttpCompressionConfig
    {
        public sealed class SupportedMethodsElement : ConfigurationElement
        {
            private const string CollectionConfigName = "";

            [ConfigurationProperty(CollectionConfigName, IsDefaultCollection = true)]
            public Collection Items
            {
                get { return (Collection)this[CollectionConfigName]; }
                set { this[CollectionConfigName] = value; }
            }

            public sealed class Collection : ConfigurationCollectionBase<CollectionItem>
            {
            }

            public sealed class CollectionItem : ConfigurationElement, IConfigurationCollectionItem
            {
                private readonly ConfigurationProperty methodProperty;

                object IConfigurationCollectionItem.Key
                {
                    get { return this.Method; }
                }

                public string Method
                {
                    get { return (string)base[this.methodProperty]; }
                    set { base[this.methodProperty] = value; }
                }

                public CollectionItem()
                {
                    this.methodProperty = new ConfigurationProperty(
                        "method",
                        typeof(string),
                        null,
                        null,
                        new StringValidator(1),
                        ConfigurationPropertyOptions.IsKey | ConfigurationPropertyOptions.IsRequired);
                    this.Properties.Add(this.methodProperty);
                }
            }
        }
    }
}
