﻿using System;
using System.Configuration;
using System.Threading;
using OneAccomplishment.Site.Core.Configuration.Validators;

namespace OneAccomplishment.Site.Core.Configuration
{
    internal sealed class CachingConfig : ConfigurationSection, ICachingConfig
    {
        private readonly ConfigurationProperty globalDurationProperty;
        private readonly ConfigurationProperty tempDataDurationProperty;
        private readonly Lazy<TimeSpan> globalDurationValue;
        private readonly Lazy<TimeSpan> tempDataDurationValue;

        TimeSpan ICachingConfig.GlobalDuration
        {
            get { return this.globalDurationValue.Value; }
        }

        TimeSpan ICachingConfig.TempDataDuration
        {
            get { return this.tempDataDurationValue.Value; }
        }

        public CachingConfig()
        {
            this.globalDurationProperty = new ConfigurationProperty(
                "globalDuration",
                typeof(double),
                null,
                null,
                new DoubleValidator(0.01, Double.MaxValue),
                ConfigurationPropertyOptions.IsRequired);
            this.tempDataDurationProperty = new ConfigurationProperty(
                "tempDataDuration",
                typeof(double),
                null,
                null,
                new DoubleValidator(0.01, Double.MaxValue),
                ConfigurationPropertyOptions.IsRequired);
            this.Properties.Add(this.globalDurationProperty);
            this.Properties.Add(this.tempDataDurationProperty);

            this.globalDurationValue = new Lazy<TimeSpan>(
                this.ConvertDoubleToTimeSpanMinutes(() => this.GlobalDuration),
                LazyThreadSafetyMode.ExecutionAndPublication);
            this.tempDataDurationValue = new Lazy<TimeSpan>(
                this.ConvertDoubleToTimeSpanMinutes(() => this.TempDataDuration),
                LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public double GlobalDuration
        {
            get { return (double)base[this.globalDurationProperty]; }
            set { base[this.tempDataDurationProperty] = value; }
        }

        public double TempDataDuration
        {
            get { return (double)base[this.tempDataDurationProperty]; }
            set { base[this.tempDataDurationProperty] = value; }
        }

        private Func<TimeSpan> ConvertDoubleToTimeSpanMinutes(Func<double> value)
        {
            return () => TimeSpan.FromMinutes(value());
        }
    }
}
