﻿using System;

namespace OneAccomplishment.Site.Core.Configuration
{
    /// <summary>
    /// Provides configuration values for caching.
    /// </summary>
    public interface ICachingConfig
    {
        /// <summary>Gets the global cache duration in minutes.</summary>
        /// <value>The global cache duration.</value>
        TimeSpan GlobalDuration { get; }
        /// <summary>Gets the TempData object cache duration in minutes.</summary>
        /// <value>The TempData object cache duration.</value>
        TimeSpan TempDataDuration { get; }
    }
}
