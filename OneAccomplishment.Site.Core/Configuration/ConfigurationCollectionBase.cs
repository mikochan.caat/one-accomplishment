﻿using System.Collections.Generic;
using System.Configuration;

namespace OneAccomplishment.Site.Core.Configuration
{
    internal abstract class ConfigurationCollectionBase<T> : ConfigurationElementCollection, IEnumerable<T>
        where T : ConfigurationElement, IConfigurationCollectionItem, new()
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((IConfigurationCollectionItem)element).Key;
        }

        public new IEnumerator<T> GetEnumerator()
        {
            var enumerator = base.GetEnumerator();
            while (enumerator.MoveNext()) {
                yield return (T)enumerator.Current;
            }
        }
    }
}
