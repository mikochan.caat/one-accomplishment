﻿namespace OneAccomplishment.Site.ViewModels
{
    /// <summary>
    /// Marker interface for all MVC ViewModels
    /// </summary>
    public interface IViewModel
    {
    }
}
