﻿using FluentValidation;
using OneAccomplishment.Site.Core.Localization;
using OneAccomplishment.Site.ViewModels.Validators;

namespace OneAccomplishment.Site.ViewModels
{
    public sealed class LoginViewModel : IViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsStayLoggedIn { get; set; }

        public sealed class Validator : AbstractUIValidator<LoginViewModel>
        {
            public Validator()
            {
                this.RuleFor(m => m.Username)
                    .NotEmpty()
                    .WithMessage(ValidationMessages.Required);
                this.RuleFor(m => m.Password)
                    .NotEmpty()
                    .WithMessage(ValidationMessages.Required);
            }
        }
    }
}
