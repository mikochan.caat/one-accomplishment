﻿using System;
using System.Web.Mvc;
using FluentValidation;

namespace OneAccomplishment.Site.ViewModels.Validators
{
    public sealed class UIValidatorFactory : IValidatorFactory
    {
        public IValidator<T> GetValidator<T>()
        {
            return (IValidator<T>)this.GetValidator(typeof(T));
        }

        public IValidator GetValidator(Type type)
        {
            var resolver = DependencyResolver.Current;
            var uiValidatorType = typeof(AbstractUIValidator<>).MakeGenericType(type);
            return (IValidator)resolver.GetService(uiValidatorType);
        }
    }
}
