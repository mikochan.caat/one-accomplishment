﻿using FluentValidation;

namespace OneAccomplishment.Site.ViewModels.Validators
{
    public abstract class AbstractUIValidator<T> : AbstractValidator<T> where T : IViewModel
    {
    }
}
