﻿using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Site.ViewModels.Shared
{
    public partial class NavItemsViewModel
    {
        public sealed class AdministrationMenuViewModel : IViewModel
        {
            [PermissionFilter(Permissions.Administration.ReadRole)]
            public bool IsRolesAccessible { get; set; }
            public bool IsUserSessionsAccessible { get; set; }
            public bool IsAuditTrailAccessible { get; set; }
            public bool IsSystemConfigurationAccessible { get; set; }
        }
    }
}
