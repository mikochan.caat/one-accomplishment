﻿using System.Web.Mvc;
using OneAccomplishment.Site.Filters;
using WebMarkupMin.AspNet4.Mvc;

namespace OneAccomplishment.Site
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // Authorization
            filters.Add(new AuthorizeAccessAttribute());
            filters.Add(new PermissionsAuthorizeAttribute());

            // Core
            filters.Add(new AnonymousOnlyCacheAttribute());
            filters.Add(new CorrelateTempDataAttribute());

            // Optimizers
            filters.Add(new MinifyHtmlAttribute());
            filters.Add(new MinifyXmlAttribute());

            // Error handlers
            filters.Add(new HandleErrorAttribute());
        }
    }
}
