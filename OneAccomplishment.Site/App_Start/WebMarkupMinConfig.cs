﻿using System.Collections.Generic;
using OneAccomplishment.Site.Core.Configuration;
using WebMarkupMin.AspNet.Brotli;
using WebMarkupMin.AspNet.Common.Compressors;
using WebMarkupMin.AspNet4.Common;

namespace OneAccomplishment.Site
{
    public static class WebMarkupMinConfig
    {
        public static void Configure()
        {
            var webMarkupConfig = WebMarkupMinConfiguration.Instance;
            webMarkupConfig.DisablePoweredByHttpHeaders = true;

            var htmlConfig = HtmlMinificationManager.Current;
            htmlConfig.SupportedHttpMethods = SiteConfiguration.HttpCompression.SupportedMethods;

            var httpConfig = HttpCompressionManager.Current;
            httpConfig.SupportedHttpMethods = SiteConfiguration.HttpCompression.SupportedMethods;
            httpConfig.CompressorFactories = new List<ICompressorFactory> {
                new BrotliCompressorFactory(),
                new DeflateCompressorFactory(),
                new GZipCompressorFactory(),
            };
        }
    }
}
