﻿using System.Web.Mvc;
using FluentValidation.Mvc;
using OneAccomplishment.Site.ViewModels.Validators;

namespace OneAccomplishment.Site
{
    public static class UIValidationConfig
    {
        public static void Configure()
        {
            var provider = new FluentValidationModelValidatorProvider(new UIValidatorFactory());

            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            ModelValidatorProviders.Providers.Add(provider);
        }
    }
}
