﻿using System.Web.Mvc;
using System.Web.Routing;

namespace OneAccomplishment.Site
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { 
                    controller = MVC.Default.Name, 
                    action = MVC.Default.ActionNames.Index, 
                    id = UrlParameter.Optional
                }
            );
        }
    }
}
