﻿using JavaScriptEngineSwitcher.ChakraCore;
using JavaScriptEngineSwitcher.Core;

namespace OneAccomplishment.Site
{
    public sealed class JsEngineSwitcherConfig
    {
        public static void Configure(IJsEngineSwitcher engineSwitcher)
        {
            engineSwitcher.EngineFactories.AddChakraCore();
            engineSwitcher.DefaultEngineName = ChakraCoreJsEngine.EngineName;
        }
    }
}
