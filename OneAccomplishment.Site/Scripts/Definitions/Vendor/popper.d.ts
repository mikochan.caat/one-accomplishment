/**
 * @fileoverview This file only declares the public portions of the API.
 * It should not define internal pieces such as utils or modifier details.
 *
 * Original definitions by: edcarroll <https://github.com/edcarroll>, ggray <https://github.com/giladgray>, rhysd <https://rhysd.github.io>, joscha <https://github.com/joscha>, seckardt <https://github.com/seckardt>, marcfallows <https://github.com/marcfallows>
 */

/**
 * This kind of namespace declaration is not necessary, but is kept here for backwards-compatibility with
 * popper.js 1.x. It can be removed in 2.x so that the default is simply the Popper class
 * and all the types / interfaces are top-level named exports.
 */
declare namespace Popper {
    type Position = 'top' | 'right' | 'bottom' | 'left';

    type Placement = 'auto-start'
        | 'auto'
        | 'auto-end'
        | 'top-start'
        | 'top'
        | 'top-end'
        | 'right-start'
        | 'right'
        | 'right-end'
        | 'bottom-end'
        | 'bottom'
        | 'bottom-start'
        | 'left-end'
        | 'left'
        | 'left-start';

    type Boundary = 'scrollParent' | 'viewport' | 'window';

    type Behavior = 'flip' | 'clockwise' | 'counterclockwise';

    type ModifierFn = (data: Data, options: Object) => Data;

    interface Attributes {
        'x-out-of-boundaries': '' | false;
        'x-placement': Placement;
    }

    interface Padding {
        top?: number,
        bottom?: number,
        left?: number,
        right?: number,
    }

    interface BaseModifier {
        order?: number;
        enabled?: boolean;
        fn?: ModifierFn;
    }

    interface Modifiers {
        shift?: BaseModifier;
        offset?: BaseModifier & {
            offset?: number | string,
        };
        preventOverflow?: BaseModifier & {
            priority?: Position[],
            padding?: number | Padding,
            boundariesElement?: Boundary | Element,
            escapeWithReference?: boolean
        };
        keepTogether?: BaseModifier;
        arrow?: BaseModifier & {
            element?: string | Element,
        };
        flip?: BaseModifier & {
            behavior?: Behavior | Position[],
            padding?: number | Padding,
            boundariesElement?: Boundary | Element,
        };
        inner?: BaseModifier;
        hide?: BaseModifier;
        applyStyle?: BaseModifier & {
            onLoad?: Function,
            gpuAcceleration?: boolean,
        };
        computeStyle?: BaseModifier & {
            gpuAcceleration?: boolean;
            x?: 'bottom' | 'top',
            y?: 'left' | 'right'
        };

        [name: string]: (BaseModifier & Record<string, any>) | undefined;
    }

    interface Offset {
        top: number;
        left: number;
        width: number;
        height: number;
    }

    interface Data {
        instance: Popper;
        placement: Placement;
        originalPlacement: Placement;
        flipped: boolean;
        hide: boolean;
        arrowElement: Element;
        styles: CSSStyleDeclaration;
        arrowStyles: CSSStyleDeclaration;
        attributes: Attributes;
        boundaries: Object;
        offsets: {
            popper: Offset,
            reference: Offset,
            arrow: {
                top: number,
                left: number,
            },
        };
    }

    interface PopperOptions {
        placement?: Placement;
        positionFixed?: boolean;
        eventsEnabled?: boolean;
        modifiers?: Modifiers;
        removeOnDestroy?: boolean;

        onCreate?(data: Data): void;

        onUpdate?(data: Data): void;
    }

    interface ReferenceObject {
        clientHeight: number;
        clientWidth: number;

        getBoundingClientRect(): ClientRect;
    }
}

declare class Popper {
    static modifiers: (Popper.BaseModifier & { name: string })[];
    static placements: Popper.Placement[];
    static Defaults: Popper.PopperOptions;

    options: Popper.PopperOptions;

    constructor(reference: Element | Popper.ReferenceObject, popper: Element, options?: Popper.PopperOptions);

    destroy(): void;

    update(): void;

    scheduleUpdate(): void;

    enableEventListeners(): void;

    disableEventListeners(): void;
}
