'use strict';

interface TippyHelper {
    (): (ref: Element) => Element | string
}

(() => {
    define(AMD.Helpers.TIPPYHELPER, [], onModuleLoad);

    function onModuleLoad() {
        return <TippyHelper>(() => (ref) => {
            let tippyContent : Element | string = '';
            const templateId = ref.getAttribute('data-template') || '';
            const templateFragment = <HTMLTemplateElement>document.getElementById(templateId);
            if (templateFragment) {
                const container = document.createElement('div');
                const templateNode = document.importNode(templateFragment.content, true);
                container.appendChild(templateNode);
                tippyContent = container;
            } else {
                console.warn(`Could not find tooltip template with id '${templateId}'`);
            }
            return tippyContent;
        });
    }
})();
