'use strict';

interface SiteScriptModule {
    (): void
}

(() => {
    const modulesToLoad = [
        AMD.Helpers.REQUIRE_EXTENSIONS,     // Ambient load
        AMD.Vendor.TIPPY,                   // Ambient load
        AMD.Site.Shared.LAYOUT,
        AMD.Vendor.DOMREADY,
    ];
    require(modulesToLoad, (_r: Require, _t: TippyJs.Tippy, layoutScript: SiteScriptModule, domReady: DomReady) => {
        domReady(() => {
            layoutScript();
        });
        const mainScriptModuleName = getMainScriptModuleName();
        if (mainScriptModuleName) {
            domReady(() => {
                loadMainScript(mainScriptModuleName);
            });
        }
    });

    function getMainScriptModuleName(): string | null {
        const mainScriptNode = document.querySelector('meta[data-main-script]');
        if (mainScriptNode) {
            const targetScriptValue = mainScriptNode.getAttribute('data-main-script');
            if (targetScriptValue) {
                return targetScriptValue;
            } else {
                console.error('A main script module node was declared but contains no ' +
                    'data-main-script attribute. Script will terminate here.');
            }
        } else {
            console.warn('No main script module was declared. Script will terminate here.');
        }
        return null;
    }

    async function loadMainScript(mainScriptModuleName: string) {
        const target = await require.module<SiteScriptModule>(mainScriptModuleName);
        if (typeof target === 'function') {
            target();
        } else {
            console.error('Target main script module is not a function. Script will terminate here.');
        }
    }
})();
