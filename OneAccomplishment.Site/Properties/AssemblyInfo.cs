﻿using System.Reflection;
using System.Runtime.InteropServices;
using OneAccomplishment.Site;
using WebActivatorEx;

[assembly: AssemblyTitle("One Accomplishment .NET")]
[assembly: AssemblyDescription("TMS task accomplishments generator")]
[assembly: AssemblyProduct("OneAccomplishment")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("83cf2775-37ae-436a-b181-686bc40cb56d")]

[assembly: AssemblyVersion("1.0.*")]

// App_Start
[assembly: PostApplicationStartMethod(typeof(RazorGeneratorMvcStart), "Start")]
