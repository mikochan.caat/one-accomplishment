﻿using System.Web.Mvc;
using OneAccomplishment.Core.Services.Auth;
using OneAccomplishment.Site.Core.Mvc.Attributes;

namespace OneAccomplishment.Site.Controllers
{
    [PermissionFilter(Permissions.Administration.ReadRole)]
    public partial class HomeController : Controller
    {
        [NoReturnUrl]
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}
