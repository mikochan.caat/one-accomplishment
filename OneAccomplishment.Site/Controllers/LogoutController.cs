﻿using System.Web.Mvc;
using System.Web.Routing;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Messages;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Commands.Logins;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Site.Core.Mvc;
using OneAccomplishment.Site.Core.Mvc.Attributes;

namespace OneAccomplishment.Site.Controllers
{
    [AllowAnonymous]
    public partial class LogoutController : Controller
    {
        private readonly ICommandHandler<LogoutAttemptCommand> logoutHandler;
        private readonly IAuthenticationService authService;

        public LogoutController(
            ICommandHandler<LogoutAttemptCommand> logoutHandler,
            IAuthenticationService authService)
        {
            this.logoutHandler = logoutHandler;
            this.authService = authService;
        }

        [DisableAnonymousCache]
        public virtual ActionResult Index(string returnUrl)
        {
            var authTicket = this.authService.CurrentTicket;
            string logoutReason;
            if (authTicket.IsExpired) {
                logoutReason = LoginMessages.LogoutReasonExpired;
            } else {
                logoutReason = LoginMessages.LogoutReasonManual;
            }

            try {
                this.logoutHandler.Handle(new LogoutAttemptCommand {
                    AuthenticationTicket = authTicket,
                    Reason = logoutReason
                });
            } catch (BusinessRuleViolationException<LoginViolations> ex) {
                if (ex.ViolatedRule != LoginViolations.CurrentAuthenticationInvalid) {
                    throw;
                }
            }

            var routeData = new RouteValueDictionary {
                { ReturnUrlHelper.Name, returnUrl }
            };
            string actionName;
            if (authTicket.IsExpired && authTicket.IsValidTicket) {
                routeData.Add("username", authTicket.Username);
                actionName = MVC.Login.ActionNames.Reauthenticate;
            } else {
                actionName = MVC.Login.ActionNames.Index;
            }
            return this.RedirectToAction(actionName, MVC.Login.Name, routeData);
        }
    }
}
