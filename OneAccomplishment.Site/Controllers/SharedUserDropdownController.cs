﻿using System.Linq;
using System.Web.Mvc;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Site.ViewModels.Shared;

namespace OneAccomplishment.Site.Controllers
{
    [ChildActionOnly]
    public partial class SharedUserDropdownController : Controller
    {
        private readonly IPrincipalService principalService;

        public SharedUserDropdownController(IPrincipalService principalService)
        {
            this.principalService = principalService;
        }

        public virtual ActionResult Normal()
        {
            return this.PartialView(MVC.Shared.Views.Controls._UserDropdown, this.CreateModel());
        }

        public virtual ActionResult Fallback()
        {
            return this.PartialView(MVC.Shared.Views.Controls._UserDropdown_Fallback, this.CreateModel());
        }

        private UserDropdownViewModel CreateModel()
        {
            var principal = this.principalService.CurrentPrincipal;
            var domainInfo = principal.DomainInfo;
            return new UserDropdownViewModel {
                Username = principal.Username,
                Initials = domainInfo.Initials,
                DisplayName = domainInfo.FullName,
                EmailAddress = domainInfo.EmailAddress,
                FirstRoleName = principal.Roles
                    .DefaultIfEmpty("<None Assigned>")
                    .First(),
                RoleCount = principal.Roles.Count,
                AdditionalRoleCount = principal.Roles.Count - 1,
                HasMoreThanOneRole = principal.Roles.Count > 1,
            };
        }
    }
}
