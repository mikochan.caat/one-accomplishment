﻿using System;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Fakes.Services
{
    internal sealed class FakeDomainService : IDomainService
    {
        private static readonly Random LetterGenerator = new Random();
        private static readonly IDomainUserInfo DefaultInfo = new FakeDomainUserInfo {
            Username = "one_user",
            EmailAddress = "one_user@oneaccomplishment.com",
            FullName = "One A. User",
            Initials = "O" + (char)LetterGenerator.Next(0x41, 0x5A + 1)
        };

        bool IDomainService.IsUsernameExisting(string username)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();

            return username == "one_user";
        }

        bool IDomainService.ValidateCredentials(string username, string password)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();
            ArgumentGuard.For(() => password).IsNull().Throw();

            return username == "one_user" && password == "test";
        }

        IDomainUserInfo IDomainService.GetDomainUserInfo(string username)
        {
            ArgumentGuard.For(() => username).IsNull().Throw();

            if (username == "one_user") {
                return DefaultInfo;
            }

            return new FakeDomainUserInfo {
                Username = username,
                EmailAddress = String.Empty,
                FullName = String.Empty,
                Initials = String.Empty
            };
        }

        private sealed class FakeDomainUserInfo : IDomainUserInfo
        {
            public string Username { get; set; }
            public string EmailAddress { get; set; }
            public string FullName { get; set; }
            public string Initials { get; set; }
        }
    }
}
