#Requires -Version 3.0

Invoke-Command -ScriptBlock {
    $SolutionNode = Get-Interface $dte.Solution ([EnvDTE80.Solution2])
    (Get-Item $SolutionNode.FullName).LastWriteTime = Get-Date
}
