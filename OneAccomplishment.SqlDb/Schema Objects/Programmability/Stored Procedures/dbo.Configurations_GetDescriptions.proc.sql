﻿CREATE PROCEDURE [dbo].[Configurations_GetDescriptions]
AS
    SET NOCOUNT ON;

    SELECT Name, [Description]
    FROM [Configurations]
GO
