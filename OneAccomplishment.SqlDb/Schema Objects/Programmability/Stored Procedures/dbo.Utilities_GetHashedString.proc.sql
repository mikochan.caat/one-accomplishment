﻿CREATE PROCEDURE [dbo].[Utilities_GetHashedString]
	@StringToHash varchar(MAX),
	@HashedValue binary(20) OUTPUT
AS
	SET @HashedValue = HASHBYTES('SHA1', @StringToHash)
GO
