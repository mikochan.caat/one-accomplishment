﻿CREATE PROCEDURE [dbo].[Permissions_GetEnumMetadata]
AS
    SET NOCOUNT ON;
    
    DECLARE @CurrentCategory AS varchar(50)
    
    DECLARE crs_Categories 
    CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT DISTINCT Category
    FROM [Permissions]
    
    OPEN crs_Categories
    FETCH NEXT FROM crs_Categories INTO @CurrentCategory
    
    WHILE @@FETCH_STATUS = 0  
    BEGIN  
        SELECT @CurrentCategory AS CurrentCategory
        SELECT PermissionName AS Name, PermissionId AS Value, [Description] AS [Description]
        FROM [Permissions]
        WHERE Category = @CurrentCategory
        ORDER BY PermissionId
        
        FETCH NEXT FROM crs_Categories INTO @CurrentCategory
    END 

    CLOSE crs_Categories  
    DEALLOCATE crs_Categories 
GO
