﻿CREATE PROCEDURE [dbo].[Configurations_GetValues]
AS
BEGIN
    SET NOCOUNT ON;
    SET FMTONLY OFF;
    
    DECLARE @configXmlPart AS xml = (
        SELECT '<' + Name + '>' + (SELECT CAST(Value AS sql_variant) FOR XML PATH('')) + '</' + Name + '>'
        FROM [Configurations]
        ORDER BY Name
        FOR XML PATH(''), TYPE
    )
    DECLARE @configXmlFull AS xml = 
        CAST('<Root><Config>' + @configXmlPart.value('.', 'nvarchar(MAX)') + '</Config></Root>' AS xml)
    
    DECLARE @xmlQueryColumns AS nvarchar(MAX) 
    ;WITH cte_XmlQueryColumns([Columns])
    AS
    (
        SELECT ', x.r.value(''' + '(' + Name + ')[1]'', ''' + DataType + ''') AS ' + QUOTENAME(Name)
        FROM [Configurations]
        ORDER BY Name
        FOR XML PATH(''), TYPE
    )
    SELECT @xmlQueryColumns = STUFF((SELECT * FROM cte_XmlQueryColumns).value('.', 'nvarchar(MAX)'), 1, 2, '')
    
    DECLARE @configQuery AS nvarchar(MAX) = 
        'SELECT ' + @xmlQueryColumns + ' 
         FROM @xml.nodes(''/Root/Config'') as x(r)'
    EXECUTE sp_executesql @configQuery, N'@xml xml', @configXmlFull
END
GO
