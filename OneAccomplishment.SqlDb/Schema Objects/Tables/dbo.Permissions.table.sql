﻿/**
 * This table provides all capabilities available when accessing a resource.
 * This is the source of truth for all authorization-based access.
 */
CREATE TABLE [dbo].[Permissions]
(
	PermissionId bigint NOT NULL IDENTITY(1, 1),
	PermissionName varchar(50) NOT NULL,
	Category varchar(50) NOT NULL,
	[Description] varchar(256) NOT NULL

	CONSTRAINT PK_Permissions
		PRIMARY KEY CLUSTERED (PermissionId)

	CONSTRAINT CHK_Permissions_PermissionName
		CHECK (LEN(PermissionName) > 0 AND PermissionName NOT LIKE '% %')
	CONSTRAINT CHK_Permissions_Category
		CHECK (LEN(Category) > 0 AND Category NOT LIKE '% %')
)
GO

CREATE UNIQUE INDEX UX_Permissions_PermissionName_Category
	ON [Permissions](PermissionName, Category)
GO

CREATE NONCLUSTERED INDEX IX_Permissions_Category
	ON [Permissions](Category)
	INCLUDE (PermissionName)
GO
