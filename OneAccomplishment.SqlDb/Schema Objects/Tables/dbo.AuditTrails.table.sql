﻿CREATE TABLE [dbo].[AuditTrails]
(
	AuditId bigint NOT NULL IDENTITY(1, 1),
	AuditDateUtc datetime NOT NULL,
	Username varchar(104) NOT NULL,				-- Max logon name length, see http://technet.microsoft.com/en-us/library/bb726984.aspx
	IpAddress varchar(45) NOT NULL,				-- Max IPv6 length
	UserAgentId bigint NOT NULL,
	AuditMessage varchar(256) NOT NULL,
	CommandType varchar(MAX) NOT NULL,			-- Command name
	Trace nvarchar(MAX) NOT NULL,				-- JSON data
	LoginRefId bigint NOT NULL					-- LoginId only as historic reference (no FK)
	
	CONSTRAINT PK_AuditTrails_AuditId
		PRIMARY KEY CLUSTERED (AuditId)
	CONSTRAINT FK_AuditTrails_UserAgents
		FOREIGN KEY (UserAgentId) REFERENCES UserAgents(UserAgentId)
		
	CONSTRAINT CHK_AuditTrails_LoginRefId
		CHECK (LoginRefId >= 0)
	CONSTRAINT CHK_AuditTrails_CommandType
		CHECK (LEN(CommandType) > 0)
)
GO

CREATE NONCLUSTERED INDEX IX_AuditTrails_LoginRefId
	ON AuditTrails(LoginRefId)
	INCLUDE (Username, IpAddress, AuditMessage)
GO

CREATE NONCLUSTERED INDEX IX_AuditTrails_Username_IpAddress
	ON AuditTrails(Username, IpAddress)
	INCLUDE (LoginRefId, AuditMessage)
GO
