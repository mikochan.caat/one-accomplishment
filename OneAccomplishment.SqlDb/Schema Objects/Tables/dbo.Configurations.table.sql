﻿CREATE TABLE [dbo].[Configurations]
(
	ConfigurationId bigint NOT NULL IDENTITY(1, 1),
	Name varchar(50) NOT NULL, 
	DataType varchar(20) NOT NULL,
	Value sql_variant NOT NULL,
	[Description] varchar(256) NOT NULL
	
	CONSTRAINT PK_Configurations
		PRIMARY KEY CLUSTERED (ConfigurationId)
		
	CONSTRAINT CHK_Configurations_Name
		CHECK (Name NOT LIKE '%[^a-z0-9_*]%' AND LEN(Name) > 0)
	CONSTRAINT CHK_Configurations_DataType
		CHECK (DataType IN (
				'bigint', 'int', 'smallint', 'tinyint', 'bit', 
				'nvarchar(MAX)', 'float', 
				'datetime', 'date', 'datetimeoffset'
			   ) AND LEN(DataType) > 0)
)
GO

CREATE UNIQUE INDEX UX_Configurations_Name
	ON [Configurations](Name)
	INCLUDE (Value, DataType)
GO
