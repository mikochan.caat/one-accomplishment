﻿/**
 * This table provides batch permission granting per user login.
 */
CREATE TABLE [dbo].[LoginRoleMappings]
(
	LoginRoleMappingId bigint NOT NULL IDENTITY(1, 1),
	LoginId bigint NOT NULL,
	RoleId bigint NOT NULL

	CONSTRAINT PK_LoginRoleMappings
		PRIMARY KEY CLUSTERED (LoginRoleMappingId)
	CONSTRAINT FK_LoginRoleMappings_Logins
		FOREIGN KEY (LoginId) REFERENCES Logins(LoginId)
	CONSTRAINT FK_LoginRoleMappings_Roles
		FOREIGN KEY (RoleId) REFERENCES Roles(RoleId)
)
GO

CREATE UNIQUE INDEX UX_LoginRoleMappings_LoginId_RoleId
	ON LoginRoleMappings(LoginId, RoleId)
GO
