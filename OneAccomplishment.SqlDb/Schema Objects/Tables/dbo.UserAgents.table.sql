﻿CREATE TABLE [dbo].[UserAgents]
(
	UserAgentId bigint NOT NULL IDENTITY(1, 1),
	UAString varchar(MAX) NOT NULL,
	HashedValue AS CAST(HASHBYTES('SHA1', UAString) AS binary(20)) PERSISTED NOT NULL

	CONSTRAINT PK_UserAgents
		PRIMARY KEY CLUSTERED (UserAgentId)
)
GO

CREATE UNIQUE INDEX UX_UserAgents_HashedValue
	ON UserAgents(HashedValue)
	INCLUDE (UAString)
GO
