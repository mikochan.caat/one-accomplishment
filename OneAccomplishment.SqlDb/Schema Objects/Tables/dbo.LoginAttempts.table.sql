﻿CREATE TABLE [dbo].[LoginAttempts]
(
	LoginAttemptId bigint NOT NULL IDENTITY(1, 1),
	LoginId bigint NOT NULL,
	IpAddress varchar(45) NOT NULL,					-- Max IPv6 length
	AttemptCount int NOT NULL,
	LastAttemptUtc datetime NOT NULL

	CONSTRAINT PK_LoginAttempts
		PRIMARY KEY CLUSTERED (LoginAttemptId)
	CONSTRAINT FK_LoginAttempts_Logins
		FOREIGN KEY (LoginId) REFERENCES Logins(LoginId)
	
	CONSTRAINT CHK_LoginAttempts_AttemptCount
		CHECK (AttemptCount >= 0)
)
GO

CREATE UNIQUE INDEX UX_LoginAttempts_LoginId_IpAddress
	ON LoginAttempts(LoginId, IpAddress)
	INCLUDE (AttemptCount)
GO
