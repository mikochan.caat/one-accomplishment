﻿-- =============================================
-- Setup mapping of Users and database roles
-- =============================================
PRINT 'Setting up database roles and users mapping...'
GO

-- Database-level permissions
GRANT DELETE TO [dbo_executor]
GRANT EXECUTE TO [dbo_executor]
GRANT INSERT TO [dbo_executor]
GRANT SELECT TO [dbo_executor]
GRANT UPDATE TO [dbo_executor]
GO

-- Table-level permissions
DECLARE @dbUsers AS TABLE(Username nvarchar(MAX))
INSERT INTO @dbUsers
SELECT Name 
FROM [dbo].[sysusers]

-- IIS APPPOOL\DefaultAppPool
IF (EXISTS(SELECT 1 FROM @dbUsers WHERE Username = 'IIS APPPOOL\DefaultAppPool'))
BEGIN
	EXEC sp_addrolemember N'dbo_executor', N'IIS APPPOOL\DefaultAppPool'
	DENY ALTER ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY CONTROL ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY DELETE ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY INSERT ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY REFERENCES ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY SELECT ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY TAKE OWNERSHIP ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY UPDATE ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY VIEW CHANGE TRACKING ON [dbo].[__RefactorLog] TO [dbo_executor]
	DENY VIEW DEFINITION ON [dbo].[__RefactorLog] TO [dbo_executor]
END
GO
