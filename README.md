## OneAccomplishment

This is my toy website using ASP.NET MVC 4.  
This project only builds in Visual Studio 2010 SP1 with other required extensions.

BUT! This project has allowed me to work with the following concepts:
- MVC
- Dependency Injection and Inversion of Control
- CQS (Command Query Separation)
- Decorator Pattern
- Unit testing and mocking
- Code Generation (Using T4 and [Reegenerator](http://www.reegenerator.com/Home.aspx/Code-Generator))
